###############################################################################
#
# Makefile hades.src.mk - to be included from the global Makefiles.
#
###############################################################################


.PHONY: install-src
install-src:
	$(INSTALL) -D -m 644 $(DIELEANA_FILE) $(INSTALL_DIR)/$(DIELEANA_FILE)
	$(INSTALL) -D -m 644 $(HELPERFC_FILE) $(INSTALL_DIR)/$(HELPERFC_FILE)
	$(INSTALL) -D -m 644 selectFunctions.h $(INSTALL_DIR)/selectFunctions.h
	@$(ECHO) -e "\n==> Sources for the application \033[1m$(APP_NAME)\033[0m installed.\n"

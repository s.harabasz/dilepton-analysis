#ifndef FIT_FUNCTIONS
#define FIT_FUNCTIONS

Double_t ExpFitFunc2S(Double_t *x, Double_t *par) {  //1/mt**2 dN/dmt fit with 2 slopes

  Double_t c    = par[0]; // normalizacja
  Double_t frac = par[1]; // frakcja
  Double_t T1   = par[2];
  Double_t T2   = par[3];
  Double_t mt   = x[0];

  return c*frac*exp(-mt/T1) + c*(1-frac)*exp(-mt/T2);
         
}

Double_t ExpFitFunc1S(Double_t* xval, Double_t* par)
{
    Double_t x, y, c, T;
    c = par[0];
    T = par[1];
    x = xval[0];
    y = c*TMath::Exp(-x/T);
    return y;
}      

Double_t ExpFitFunc1S_m32(Double_t* xval, Double_t* par)
{
    Double_t x, y, c, T;
    c = par[0];
    T = par[1];
    x = xval[0];
    y = c*TMath::Exp(-x/T)*TMath::Power(x,3./2.);
    return y;
}      


Double_t ExpFitFunc2S_m2(Double_t *x, Double_t *par) {  //1/mt**2 dN/dmt fit with 2 slopes

  Double_t c    = par[0]; // normalizacja
  Double_t frac = par[1]; // frakcja
  Double_t T1   = par[2];
  Double_t T2   = par[3];
  Double_t mt   = x[0];

  return c*frac*exp(-mt/T1)/(mt*mt) + c*(1-frac)*exp(-mt/T2)/(mt*mt);
         
}

Double_t ExpFitFunc1S_m2(Double_t* xval, Double_t* par)
{
    Double_t x, y, c, T;
    c = par[0];
    T = par[1];
    x = xval[0];
    y = c*TMath::Exp(-x/T)/(x*x);
    return y;
}      

Double_t BesselK1(Double_t *x, Double_t *a) {
    Double_t pt = x[0];
    Double_t A = a[0];
    Double_t T = a[1];
    Double_t m = a[2];
    Double_t mt = TMath::Sqrt(pt*pt + m*m);
    Double_t K1 = TMath::BesselK1(mt/T);
   // cerr << "pt " << pt << " mt " << mt << " K1 " << K1 << endl;
    return A*pt*mt*K1;
}



#endif

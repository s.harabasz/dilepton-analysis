################################################################################
#                                                                              #
# Debian 8 Login Script for backward compatibility of HADES Software.          #
#                                                                              #
# Date: 15.03.2022                                                             #
# Author: Simon Spies, Goethe University Frankfurt                             #
#                                                                              #
################################################################################

export SSHD_CONTAINER_DEFAULT="/cvmfs/vae.gsi.de/debian8/containers/user_container-production.sif"
export SSHD_CONTAINER_OPTIONS="--bind /etc/slurm,/var/run/munge,/var/spool/slurm,/var/lib/sss/pipes/nss,/cvmfs/vae.gsi.de,/cvmfs/hadessoft.gsi.de/install/debian8/install:/cvmfs/hades.gsi.de/install,/cvmfs/hadessoft.gsi.de/param:/cvmfs/hades.gsi.de/param,/cvmfs/hadessoft.gsi.de/install/debian8/oracle:/cvmfs/it.gsi.de/oracle"

shell=$(getent passwd $USER | cut -d : -f 7)

STARTUP_COMMAND=$(cat << EOF
    srun() { srun-nowrap --singularity-no-bind-defaults "\$@"; }
    sbatch() { sbatch-nowrap --singularity-no-bind-defaults "\$@"; }
    export -f srun sbatch
    $shell -l
EOF
)

export SINGULARITYENV_PS1="\u@\h:\w > "
export SINGULARITYENV_SLURM_SINGULARITY_CONTAINER=$SSHD_CONTAINER_DEFAULT

test -f /etc/motd && cat /etc/motd
echo Container launched: $(realpath $SSHD_CONTAINER_DEFAULT)
exec singularity exec $SSHD_CONTAINER_OPTIONS $SSHD_CONTAINER_DEFAULT $shell -c "$STARTUP_COMMAND"

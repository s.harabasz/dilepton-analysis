#ifndef HIST_FUNCTIONS
#define HIST_FUNCTIONS

#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TString.h"
#include "TMath.h"
#include "TH2F.h"
#include "TH1F.h"
#include "TF1.h"

using namespace std;

void divideGraph(TGraphAsymmErrors *g, TF1 *f) {
    Int_t n = g->GetN();
    Double_t min, max;
    f->GetRange(min,max);
    for (int i = 0; i < n; ++i) {
        Double_t x = g->GetX()[i];
        Double_t y = g->GetY()[i];
        Double_t dylow = g->GetEYlow()[i];
        Double_t dyhig = g->GetEYhigh()[i];
        if (x > min && x < max) {
            g->SetPoint(i,x,y/f->Eval(x));
            g->SetPointEYlow(i,dylow/f->Eval(x));
            g->SetPointEYhigh(i,dyhig/f->Eval(x));
        }
    }
}

TH1F *profileToHist(TProfile *p, TString nameSuffix = "") {
    Int_t n = p->GetNbinsX();
    Double_t *bins = new Double_t[n+1];
    for (int i = 0; i < n; ++i) {
        bins[i] = p->GetXaxis()->GetBinLowEdge(i+1);
    }
    bins[n] = p->GetXaxis()->GetBinUpEdge(n);
    TH1F *h = new TH1F(Form("%s_hist%s",p->GetName(),nameSuffix.Data()),Form("%s_hist%s",p->GetName(),nameSuffix.Data()),n,bins);
    for (int b = 1; b <= n; ++b) {
        h->SetBinContent(b,p->GetBinContent(b));
        h->SetBinError(b,p->GetBinError(b));
    }
    return h;
}

TH1F *profileToHist(TProfile2D *p, Int_t bin, TString nameSuffix = "") {
    Int_t n = p->GetNbinsX();
    Double_t *bins = new Double_t[n+1];
    for (int i = 0; i < n; ++i) {
        bins[i] = p->GetXaxis()->GetBinLowEdge(i+1);
    }
    bins[n] = p->GetXaxis()->GetBinUpEdge(n);
    TH1F *h = new TH1F(Form("%s_hist%s",p->GetName(),nameSuffix.Data()),Form("%s_hist%s",p->GetName(),nameSuffix.Data()),n,bins);
    for (int b = 1; b <= n; ++b) {
        h->SetBinContent(b,p->GetBinContent(b,bin));
        h->SetBinError(b,p->GetBinError(b,bin));
    }
    return h;
}

Bool_t scaleGraphErrors(TGraphErrors* g,
        Double_t xscale,Double_t yscale,
        Double_t xErrscale,Double_t yErrscale)
{
    // scales x-values and y-values with  xscale,yscale.
    // scales x Err-values and yErr-values with  xErrscale,yErrscale.
    if(g==0) return kFALSE;
    Int_t nbin=g->GetN();
    Double_t x,y;
    Double_t xErr,yErr;
    for(Int_t i=0;i<nbin;i++)
    {
        cout << i << " of " << nbin << endl;
        g->GetPoint(i,x,y);
        xErr = g->GetErrorX(i);
        yErr = g->GetErrorY(i);

        cout << x << " " << xErr << " " << y << " " << yErr << endl;

        g->SetPoint(i,x*xscale,y*yscale);
        cout << "CLASS NAME : " << g->ClassName() << endl;
        g->SetPointError(i,xErr * xErrscale, yErr * yErrscale);
    }
    return kTRUE;
}

Bool_t scaleGraphAsymmErrors(TGraphAsymmErrors* g,
        Double_t xscale,Double_t yscale,
        Double_t xErrscale,Double_t yErrscale)
{
    // scales x-values and y-values with  xscale,yscale.
    // scales x Err-values and yErr-values with  xErrscale,yErrscale.
    if(g==0) return kFALSE;
    Int_t nbin=g->GetN();
    Double_t x,y;
    Double_t xErrH,yErrH,xErrL,yErrL;
    for(Int_t i=0;i<nbin;i++)
    {
        g->GetPoint(i,x,y);
        xErrL = g->GetEXlow()[i];
        xErrH = g->GetEXhigh()[i];
        yErrL = g->GetEYlow()[i];
        yErrH = g->GetEYhigh()[i];

        g->SetPoint(i,x*xscale,y*yscale);
        g->SetPointError(i,xErrL * xErrscale, xErrH * xErrscale, yErrL * yErrscale, yErrH * yErrscale);
    }
    return kTRUE;
}

// Linear interpolation
Double_t interpolateGraph(TGraph *g, Double_t x, bool debug = false) {
    Int_t nearIlow = 0;
    Int_t nearIhig = 0;
    Double_t prevNearXlow = g->GetX()[0];
    Double_t prevNearXhig = g->GetX()[g->GetN()-1];
    for (int i = 0; i < g->GetN(); ++i) {
        Double_t xi = g->GetX()[i];
        if (xi == x) {
            return g->GetY()[i];
        }
        if (xi < x && xi > prevNearXlow) {
            nearIlow = i;
            prevNearXlow = xi;
            if (debug) cout << "  - " << i << " " << xi << endl;
        }
        if (xi > x && xi < prevNearXhig) {
            nearIhig = i;
            prevNearXhig = xi;
            if (debug) cout << "  + " << i << " " << xi << endl;
        }
    }

    if (debug) cout << "   " << x << " " << nearIlow << " " << nearIhig << endl;

    Double_t nearYlow = g->GetY()[nearIlow];
    Double_t nearYhig = g->GetY()[nearIhig];

    Double_t a = (nearYhig - nearYlow) / (prevNearXhig - prevNearXlow);
    Double_t b = nearYlow - a*prevNearXlow;

    return a*x+b;
}

Double_t nearestPointGraph(TGraph *g, Double_t x, Int_t &nearI, bool debug = false) {
    nearI = 0;
    Double_t prevDelta = TMath::Abs(x-g->GetX()[0]);
    for (int i = 0; i < g->GetN(); ++i) {
        Double_t xi = g->GetX()[i];
        Double_t delta = TMath::Abs(xi-x);
        if (xi == x) {
            return g->GetY()[i];
        }
        if (delta < prevDelta) {
            nearI = i;
            prevDelta = delta;
            if (debug) cout << "  - " << i << " " << xi << endl;
        }
    }

    if (debug) cout << "   " << x << " " << nearI << endl;

    return g->GetY()[nearI];
}

TGraphAsymmErrors* rapiditySystematics(TGraphAsymmErrors *gre, bool debug = true) {

    if (debug) cout << "->->-> ADDITIONAL SYSTEMATIC ERROR FOR RAPIDITY, GRAPH NAME " << gre->GetName() << endl;

    const Double_t midy = 0.74;
    Int_t n = gre->GetN();
    TGraphAsymmErrors *result = new TGraphAsymmErrors(n);
    Double_t maximum = 0;
    for (int i = 0; i < n; ++i) {
        Double_t temp = gre->GetY()[i] + gre->GetEYhigh()[i];
        if (temp > maximum) {
            maximum = temp;
        }
    }
    
    for (int i = 0; i < n; ++i) {
        Double_t x,y,dx,dy;
        x = gre->GetX()[i];
        y = gre->GetY()[i];
        dx = gre->GetErrorX(i);
        dy = gre->GetErrorY(i);
        result->SetPoint(i,x,y);
        if (x < midy) {
            result->SetPointError(i,dx,dx,dy,dy);
        } else {
            Double_t xprim = 2*midy-x; // midy-(x-midy)
            Int_t nearI;
            Double_t yprim = nearestPointGraph(gre,xprim,nearI);
            bool withinOneBin = false;
            if (nearI < gre->GetN()-1) {
                // xprim closer to nearest point than nearest point to the next one
                withinOneBin = TMath::Abs(gre->GetX()[nearI] - xprim) < TMath::Abs(gre->GetX()[nearI] - gre->GetX()[nearI+1]);
            } else {
                // special case, there is no next point, so compare to the previous one
                withinOneBin = TMath::Abs(gre->GetX()[nearI] - xprim) < TMath::Abs(gre->GetX()[nearI] - gre->GetX()[nearI-1]);
            }

            Double_t diff = y-yprim;
            Double_t dynew = TMath::Sqrt(dy*dy+diff*diff);
            if (xprim > 0.2 && yprim > 0 && dynew < maximum && withinOneBin) {


                if (debug) cout << x << " " << xprim << " " << y << " " << yprim << " " << diff << " " << dynew << " " << endl;

                if (diff < 0) {
                    result->SetPointError(i,dx,dx,dy,dynew);
                } else {
                    result->SetPointError(i,dx,dx,dynew,dy);
                }
            } else {
                result->SetPointError(i,dx,dx,dy,dy);
            }
        }
    }

    if (debug) cout << "->->-> " << endl;

    return result;
    
}

TGraphErrors *histToGraph(TH1F* h) {
    Int_t nbins = h->GetNbinsX();
    TGraphErrors *gre = new TGraphErrors;
    gre->SetName(Form("%s_graph",h->GetName()));
    for (int b = 1; b <= nbins; ++b) {
        Float_t center = h->GetBinCenter(b);
        Float_t content = h->GetBinContent(b);
        Float_t width = h->GetBinWidth(b);
        Float_t error = h->GetBinError(b);
        gre->SetPoint(b,center,content);
        gre->SetPointError(b,width/2,error);

    }
    return gre;
}

template <typename T>
T oneOverHist(T h1) {
    TString name(h1->GetName());
    name.Append("_Inverse");
    T result = (T)h1->Clone(name);
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= h1->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= h1->GetNbinsZ(); ++bin_z) {
                Float_t content_in  = h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t error_in    = h1->GetBinError(bin_x,bin_y,bin_z);
                Float_t content_out = 1./content_in;
                Float_t error_out   = error_in/(content_in*content_in);

                if (!TMath::Finite(content_out) || !TMath::Finite(error_out)) {
                    content_out = error_out = 0;
                }

                result->SetBinContent(bin_x,bin_y,bin_z, content_out);
                result->SetBinError(bin_x,bin_y,bin_z,error_out);
            }
        }
    }
    return result;
}

template <typename T>
T sqrtHist(T h1) {
    TString name(h1->GetName());
    name.ReplaceAll("Avg","Sqrt");
    name.ReplaceAll("Sig","Sqrt");
    name.ReplaceAll("S2B","Sqrt");
    name.ReplaceAll("NP","Sqrt");
    name.ReplaceAll("NN","Sqrt");
    name.ReplaceAll("PP","Sqrt");
    T result = (T)h1->Clone(name);
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= h1->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= h1->GetNbinsZ(); ++bin_z) {
                Float_t content_in  = h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t error_in    = h1->GetBinError(bin_x,bin_y,bin_z);
                Float_t content_out = TMath::Sqrt(content_in);
                Float_t error_out   = 0.5*error_in/TMath::Sqrt(content_in);
                result->SetBinContent(bin_x,bin_y,bin_z, content_out);
                result->SetBinError(bin_x,bin_y,bin_z,error_out);
            }
        }
    }
    return result;
}

template <typename T>
T harmAvg(T h1, T h2, Double_t minratio = .0) {
    TString name(h1->GetName());
    name.ReplaceAll("NN","HarmAvg");
    name.ReplaceAll("PP","HarmAvg");
    T result = (T)h1->Clone(name);
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= h1->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= h1->GetNbinsZ(); ++bin_z) {
                Float_t ratio1 = h1->GetBinContent(bin_x,bin_y,bin_z)/h2->GetBinContent(bin_x,bin_y,bin_z);
                Float_t ratio2 = h2->GetBinContent(bin_x,bin_y,bin_z)/h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t content = 2/(1/h1->GetBinContent(bin_x,bin_y,bin_z)+1/h2->GetBinContent(bin_x,bin_y,bin_z));
                Float_t x = h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t y = h2->GetBinContent(bin_x,bin_y,bin_z);
                Float_t sx = h1->GetBinError(bin_x,bin_y,bin_z);
                Float_t sy = h2->GetBinError(bin_x,bin_y,bin_z);
                Float_t dfdx2 = 4*TMath::Power( y / (x + y), 4 );
                Float_t dfdy2 = 4*TMath::Power( x / (x + y), 4 ); 
                Float_t error = TMath::Sqrt( dfdx2*sx*sx + dfdy2*sy*sy );
                if (ratio1 <= minratio || ratio2 <= minratio || !TMath::Finite(ratio1) || !TMath::Finite(ratio2)) {
                    content = h1->GetBinContent(bin_x,bin_y,bin_z)+h2->GetBinContent(bin_x,bin_y,bin_z);
                    error = h1->GetBinError(bin_x,bin_y,bin_z)+h2->GetBinError(bin_x,bin_y,bin_z);
                }
                result->SetBinContent(bin_x,bin_y,bin_z, content);
//                Float_t error = TMath::Sqrt(h1->GetBinContent(bin_x,bin_y,bin_z)) + TMath::Sqrt(h2->GetBinContent(bin_x,bin_y,bin_z));
//                Float_t error = h1->GetBinError(bin_x,bin_y,bin_z) + h2->GetBinError(bin_x,bin_y,bin_z);
                result->SetBinError(bin_x,bin_y,bin_z,error);
            }
        }
    }
    return result;
}

TProfile *geomAvg(TProfile *h1, TProfile *h2, Double_t minratio = .0) {
    TString name(h1->GetName());
    name.ReplaceAll("NN","Avg");
    name.ReplaceAll("PP","Avg");
    TProfile *result = (TProfile*)h1->Clone(name);
    result->Reset();
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        Float_t ratio1 = h1->GetBinContent(bin_x)/h2->GetBinContent(bin_x);
        Float_t ratio2 = h2->GetBinContent(bin_x)/h1->GetBinContent(bin_x);

        Float_t X1 = h1->GetBinContent(bin_x);
        Float_t X2 = h2->GetBinContent(bin_x);
        Float_t N1 = h1->GetBinEntries(bin_x);
        Float_t N2 = h2->GetBinEntries(bin_x);
        Float_t s1 = h1->GetBinError(bin_x);
        Float_t s2 = h2->GetBinError(bin_x);

        Float_t content = 2*TMath::Sqrt(X1*X2);
        Float_t entries = 2*TMath::Sqrt(N1*N2);
        Float_t dfdX12 = X2/X1;
        Float_t dfdX22 = X1/X2;
        Float_t error = TMath::Sqrt( dfdX12*s1*s1 + dfdX22*s2*s2 );
        if (X1 < 0 && X2 < 0) { content = -content; }

        cout << "1. " << content << " " << entries << " " << error << endl;

        if (ratio1 <= minratio || ratio2 <= minratio || !TMath::Finite(ratio1) || !TMath::Finite(ratio2)) {
            content = X1+X2;
            error = TMath::Sqrt(s1*s1+s2*s2);
            entries = N1+N2;
        }
        cout << "2. " << content << " " << entries << " " << error << endl;
        cout << "3. " << result->GetBinContent(bin_x) << " " << result->GetBinEntries(bin_x) << " " << result->GetBinError(bin_x) << endl;
        result->SetBinEntries(bin_x,entries);
        cout << "4. " << result->GetBinContent(bin_x) << " " << result->GetBinEntries(bin_x) << " " << result->GetBinError(bin_x) << endl;
        result->SetBinContent(bin_x, content*entries);
        cout << "5. " << result->GetBinContent(bin_x) << " " << result->GetBinEntries(bin_x) << " " << result->GetBinError(bin_x) << endl;
        result->SetBinError(bin_x,error);
        cout << "6. " << result->GetBinContent(bin_x) << " " << result->GetBinEntries(bin_x) << " " << result->GetBinError(bin_x) << endl;
        cout << "--" << endl;

    }
    return result;
}

template <typename T>
T geomAvg(T h1, T h2, Double_t minratio = .0) {
    TString name(h1->GetName());
    name.ReplaceAll("NN","Avg");
    name.ReplaceAll("PP","Avg");
    T result = (T)h1->Clone(name);
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= h1->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= h1->GetNbinsZ(); ++bin_z) {
                Float_t ratio1 = h1->GetBinContent(bin_x,bin_y,bin_z)/h2->GetBinContent(bin_x,bin_y,bin_z);
                Float_t ratio2 = h2->GetBinContent(bin_x,bin_y,bin_z)/h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t content = 2*TMath::Sqrt(h1->GetBinContent(bin_x,bin_y,bin_z)*h2->GetBinContent(bin_x,bin_y,bin_z));
                Float_t x = h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t y = h2->GetBinContent(bin_x,bin_y,bin_z);
                Float_t sx = h1->GetBinError(bin_x,bin_y,bin_z);
                Float_t sy = h2->GetBinError(bin_x,bin_y,bin_z);
                Float_t dfdx2 = y/x;
                Float_t dfdy2 = x/y;
                Float_t error = TMath::Sqrt( dfdx2*sx*sx + dfdy2*sy*sy );
                if (ratio1 <= minratio || ratio2 <= minratio || !TMath::Finite(ratio1) || !TMath::Finite(ratio2)) {
                    content = h1->GetBinContent(bin_x,bin_y,bin_z)+h2->GetBinContent(bin_x,bin_y,bin_z);
                    error = h1->GetBinError(bin_x,bin_y,bin_z)+h2->GetBinError(bin_x,bin_y,bin_z);
                }
                result->SetBinContent(bin_x,bin_y,bin_z, content);
//                Float_t error = TMath::Sqrt(h1->GetBinContent(bin_x,bin_y,bin_z)) + TMath::Sqrt(h2->GetBinContent(bin_x,bin_y,bin_z));
//                Float_t error = h1->GetBinError(bin_x,bin_y,bin_z) + h2->GetBinError(bin_x,bin_y,bin_z);
                result->SetBinError(bin_x,bin_y,bin_z,error);
            }
        }
    }
    return result;
}

template <typename T>
T geomAvgErrProp(T h1, T h2, Double_t minratio = .0) {
    TString name(h1->GetName());
    name.ReplaceAll("NN","Avg");
    name.ReplaceAll("PP","Avg");
    T result = (T)h1->Clone(name);
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= h1->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= h1->GetNbinsZ(); ++bin_z) {
                Float_t ratio1 = h1->GetBinContent(bin_x,bin_y,bin_z)/h2->GetBinContent(bin_x,bin_y,bin_z);
                Float_t ratio2 = h2->GetBinContent(bin_x,bin_y,bin_z)/h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t content = 2*TMath::Sqrt(h1->GetBinContent(bin_x,bin_y,bin_z)*h2->GetBinContent(bin_x,bin_y,bin_z));
                if (ratio1 <= minratio || ratio2 <= minratio) {
                    content = h1->GetBinContent(bin_x,bin_y,bin_z)+h2->GetBinContent(bin_x,bin_y,bin_z);
                }
                result->SetBinContent(bin_x,bin_y,bin_z, content);
//                Float_t error = TMath::Sqrt(h1->GetBinContent(bin_x,bin_y,bin_z)) + TMath::Sqrt(h2->GetBinContent(bin_x,bin_y,bin_z));
                
                Float_t error = h1->GetBinError(bin_x,bin_y,bin_z) + h2->GetBinError(bin_x,bin_y,bin_z);
                result->SetBinError(bin_x,bin_y,bin_z,error);
            }
        }
    }
    return result;
}

template <typename T>
T geomAvgTrans(T h1, T h2) {
    TString name(h1->GetName());
    name.ReplaceAll("NN","Avg");
    name.ReplaceAll("PP","Avg");
    T result = (T)h1->Clone(name);
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= h1->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= h1->GetNbinsZ(); ++bin_z) {
                Float_t ratio1 = h1->GetBinContent(bin_x,bin_y,bin_z)/h2->GetBinContent(bin_x,bin_y,bin_z);
                Float_t ratio2 = h2->GetBinContent(bin_x,bin_y,bin_z)/h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t low_ratio = TMath::Min(ratio1,ratio2);
                Float_t geom  = 2*TMath::Sqrt(h1->GetBinContent(bin_x,bin_y,bin_z)*h2->GetBinContent(bin_x,bin_y,bin_z));
                Float_t arith = h1->GetBinContent(bin_x,bin_y,bin_z)+h2->GetBinContent(bin_x,bin_y,bin_z);
                Float_t content = low_ratio*geom + (1-low_ratio)*arith;
//                Float_t error = TMath::Sqrt(h1->GetBinContent(bin_x,bin_y,bin_z)) + TMath::Sqrt(h2->GetBinContent(bin_x,bin_y,bin_z));
                Float_t error = h1->GetBinError(bin_x,bin_y,bin_z) + h2->GetBinError(bin_x,bin_y,bin_z);
                if (TMath::IsNaN(content)) {
                    content = 0;
                    error = 0;
                }
                result->SetBinContent(bin_x,bin_y,bin_z, content);
                result->SetBinError(bin_x,bin_y,bin_z,error);
            }
        }
    }
    return result;
}

template <typename T>
T kFactor(T np, T pp, T nn, Double_t minratio = .0) {
    TString name(np->GetName());
    name.ReplaceAll("NP","kFactor");
    T result = (T)np->Clone(name);
    result->Divide(geomAvg(pp,nn,minratio));
    return result;
}

template <typename T>
T fixK(T k, Double_t howmuch = .1) {
    for (Int_t b = 1; b <= k->GetNbinsX(); ++b) {
        Double_t content = k->GetBinContent(b);
        content = (content-1)*howmuch + 1;
        k->SetBinContent(b,content);
    }
    return k;
}

template <typename T>
T significance(T h_sig, T h_bgr) {
    TString name(h_sig->GetName());
    name.ReplaceAll("Sig","Sgn");
    T result = (T)h_sig->Clone(name);
    T sigcln = (T)h_sig->Clone("clone");
    sigcln->Add(h_bgr,2);
    T h_sqrt = sqrtHist(sigcln);
    result->Divide(h_sqrt);
    for (int b = 1; b <= result->GetNbinsX(); ++b) {
        if (!TMath::Finite(result->GetBinContent(b))) {
            result->SetBinContent(b,0);
        }
        result->SetBinError(b,0);
    }
    return result;
}
/*
 * Here wrong formula S/sqrt(S+B) instead of S/sqrt(2S+B)
template <typename T>
T significance(T h_np, T h_pp, T h_nn) {
    T h_bgr = geomAvg(h_pp,h_nn);
    T result = sqrtHist(h_np);
    TString name(h_np->GetName());
    name.ReplaceAll("NP","Sgn");
    result->SetNameTitle(name,name);
    h_bgr->Divide(result);
    result->Add(h_bgr,-1);
    return result;
}
*/
template <typename T>
T bgr(T pp, T nn, T np_mix, T pp_mix, T nn_mix) {
    T avg = geomAvg(pp,nn);
    T np_temp = (T)np_mix->Clone("np_temp");
    int bin_x_start = avg->GetXaxis()->FindBin(300.);
    int bin_x_stop = avg->GetXaxis()->FindBin(1000.);
    float integral_avg = avg->Integral(bin_x_start,bin_x_stop);
    float integral_np_temp = np_temp->Integral(bin_x_start,bin_x_stop);
    np_temp->Scale(integral_avg/integral_np_temp);

    for (int bin_x = bin_x_start; bin_x <= pp->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= pp->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= pp->GetNbinsZ(); ++bin_z) {
                avg->SetBinContent(bin_x,bin_y,bin_z,np_temp->GetBinContent(bin_x,bin_y,bin_z));
                avg->SetBinError(bin_x,bin_y,bin_z,np_temp->GetBinError(bin_x,bin_y,bin_z));
            }
        }
    }
    T avg_mix = geomAvg(pp_mix,nn_mix);
    T factor = (T)np_mix->Clone("factor");
    factor->Divide(avg_mix);

    avg->Multiply(factor);
    return avg;
}

TH1F* bgrWithMix(TH1F* avg, TH1F* mix, TH1F*& ratio, Float_t startFrom = 0., Float_t startIntegral = 0.2, Float_t endIntegral = 0.3) {
    TString histname(avg->GetName());
    histname.ReplaceAll("Avg","BgrWithMix");
    TH1F* result = (TH1F*)avg->Clone(histname);
    TH1F* avgcln = (TH1F*)avg->Clone("avgcln");
    TH1F* mixcln = (TH1F*)mix->Clone("mixcln");
/*
    avgcln->Divide(mixcln);
    
    TF1 *pol0 = new TF1("pol0","pol0",0.2,1);
    avgcln->Fit(pol0,"Q0","",0.2,1);
    ratio = avgcln;

    Double_t norm = pol0->GetParameter(0);
*/
    TString name(avg->GetName());
    if (name.Contains("pt300to450") || name.Contains("pt450to600") || name.Contains("pt600to1000")) {
        startFrom = 0;
        endIntegral = 1000;
    }

    Int_t bin1 = avgcln->FindBin(startIntegral);
    Int_t bin2 = avgcln->FindBin(endIntegral);

    Double_t num = avgcln->Integral(bin1,bin2);
    Double_t den = mixcln->Integral(bin1,bin2);
    Double_t norm = num/den;
    mixcln->Scale(norm);

/*
    cout << "===========================================================================================" << endl;
    cout << "bgrWithMix: num, den, num/den = " << num << ", " << den << ", " << num/den << endl;
    cout << "bgrWithMix: startFrom, starfIntegral, endIntegral = " << startFrom << ", " << startIntegral << ", " << endIntegral << endl;
    cout << "bgrWithMix: bin1, bin2 = " << bin1 << ", " << bin2 << endl;
    cout << "===========================================================================================" << endl;
*/

    for (int b = 1; b <= result->GetNbinsX(); ++b) {
       // cout << "  --> " << b << " " << result->GetBinCenter(b) << " " << mixcln->GetBinContent(b) << endl;
        if (result->GetBinCenter(b) > startFrom) {
            result->SetBinContent(b,mixcln->GetBinContent(b));
            result->SetBinError(b,mixcln->GetBinError(b));

        }
    }
    return result;
}

TH1F *fitBgr(TH1F* h, Float_t minFit, Float_t maxFit) {
    Float_t max = h->GetXaxis()->GetBinUpEdge(h->GetNbinsX());
    TF1 *f = new TF1("f","expo",minFit,max);
    h->Fit(f,"Q0","",minFit,maxFit);
    Int_t minBin = h->FindBin(minFit);
    Int_t maxBin = h->GetNbinsX();
    for (int b = minBin; b <= maxBin; ++b) {
        h->SetBinContent(b, f->Eval(h->GetBinCenter(b)));
        h->SetBinError(b, 0);
    }
    return h;
}

void percentError(TH1F* h, Double_t percent) {
    Int_t nbins = h->GetNbinsX();
    for (int b = 1; b <= nbins; ++b) {
        h->SetBinError(b,percent*h->GetBinContent(b));
    }
}

template<typename T>
T *adaptBins(T *h, Int_t nbins, const Double_t *xAxis,bool=false) {
    T *hRebin = (T*)h->Rebin(nbins,TString(h->GetName()) + "_rebinned",xAxis);
    hRebin->Scale(1,"width");
    return hRebin;
}
template<typename T1, typename T2>
T1 *adaptBins(T1 *h, T2 *targetBinning,bool=false) {
    return adaptBins(h, targetBinning->GetNbinsX(),targetBinning->GetXaxis()->GetXbins()->GetArray());
}


TH1F *adaptBinsFailsafe(TH1F *h, TH1F *targetBinning, bool debug = false) {
    TH1F *h_errors = (TH1F*)h->Clone(TString(h->GetName()) + "_errors");
    for (int b1 = 1; b1 <= h->GetNbinsX(); ++b1) {
        h_errors->SetBinContent(b1,h->GetBinError(b1));
    }
    TString histname = TString(h->GetName()) + "_interpolated";
    TH1F *result = (TH1F*)targetBinning->Clone(histname);
    result->SetTitle(h->GetTitle());
    TGraph *errors = new TGraph(result->GetNbinsX());
    errors->SetName("grerrors");
    for (int b2 = 1; b2 <= result->GetNbinsX(); ++b2) {
        Float_t epsilon = 0.001*result->GetXaxis()->GetBinWidth(b2);
        Float_t result_low_edge = result->GetXaxis()->GetBinLowEdge(b2);
        Float_t result_up_edge  = result->GetXaxis()->GetBinUpEdge(b2);
        if (debug) cout << "Result edges " << result_low_edge << " " << result_up_edge << endl;

       
        Int_t orig_low_bin_pe = h->FindBin(result_low_edge+epsilon);
        Int_t orig_up_bin_me  = h->FindBin(result_up_edge-epsilon);
        Int_t orig_low_bin = orig_low_bin_pe;
        Int_t orig_up_bin = orig_up_bin_me;

        Float_t orig_low_edge = h->GetXaxis()->GetBinLowEdge(orig_low_bin);
        Float_t orig_up_edge  = h->GetXaxis()->GetBinUpEdge(orig_up_bin);
        if (debug) cout << "Orig edges " << orig_low_edge << " " << orig_up_edge << endl;
        if (orig_low_bin == orig_up_bin) { // new bin within the same original bin
            Float_t bin_center = result->GetBinCenter(b2);
            result->SetBinContent(b2,h->Interpolate(bin_center));
            result->SetBinError(b2,h_errors->Interpolate(bin_center));
        } else { // new bin spans over several original bins
            Float_t bin_content = 0;
            Float_t bin_error = 0;
            for (int b_orig = orig_low_bin; b_orig <= orig_up_bin; ++b_orig) {
                Float_t b_width = h->GetBinWidth(b_orig);
                if (b_orig == orig_low_bin) {
                    Float_t overlap = h->GetXaxis()->GetBinUpEdge(b_orig) - result_low_edge;
                    bin_content += h->GetBinContent(b_orig) * overlap;
                    bin_error   += TMath::Power(h->GetBinError(b_orig),2) * overlap;
                    if (debug) cout << "< bin " << b2 << endl;
                } else if (b_orig == orig_up_bin) {
                    Float_t overlap = result_up_edge - h->GetXaxis()->GetBinLowEdge(b_orig);
                    bin_content += h->GetBinContent(b_orig) * overlap;
                    bin_error   += TMath::Power(h->GetBinError(b_orig),2) * overlap;
                    if (debug) cout << "> bin " << b2 << endl;
                } else {
                    bin_content += h->GetBinContent(b_orig) * b_width;
                    bin_error   += TMath::Power(h->GetBinError(b_orig),2) * b_width;
                }
            }
            Float_t err1 = targetBinning->GetBinError(b2);
            Float_t err2 = result->GetBinError(b2);
            Int_t nmerged = orig_up_bin - orig_low_bin + 1;

            bin_content /= result->GetBinWidth(b2);
            bin_error /= result->GetBinWidth(b2);
            result->SetBinContent(b2,bin_content);
            result->SetBinError(b2,TMath::Sqrt(bin_error/nmerged));

            if (TMath::Finite(err2/err1) && debug) {
                cout << err1 << " " << err2 << " " << err2/err1 << " " << nmerged << endl;
                errors->SetPoint(b2,TMath::Sqrt(nmerged),err2/err1);
            }
        }
    }
    if (debug) errors->SaveAs("errors.root");
    return result;
}

TH1F *adaptBinsFailsafe(TH1F *h, Int_t targetNbins, Double_t *targetAxis) {
    static TH1F *targetBinning = NULL;
    if (targetBinning == NULL) {
        targetBinning = new TH1F("targetBinning","",targetNbins,targetAxis);
    }
    return adaptBinsFailsafe(h,targetBinning);
}

TH1F *adaptBinsFailsafeA(TH1F *h, Int_t targetNbins, Double_t *targetAxis) {
    static TH1F *targetBinning = NULL;
    if (targetBinning == NULL) {
        targetBinning = new TH1F("targetBinning","",targetNbins,targetAxis);
    }
    return adaptBinsFailsafe(h,targetBinning);
}

TH2F *adaptBins(TH2F *h, TH2F *targetBinning) {
    TH2F *h_errors = (TH2F*)h->Clone("h_errors");
    for (int bx1 = 1; bx1 <= h->GetNbinsX(); ++bx1) {
        for (int by1 = 1; by1 <= h->GetNbinsY(); ++by1) {
            h_errors->SetBinContent(bx1,by1,h->GetBinError(bx1,by1));
        }
    }
    TString histname = TString(h->GetName()) + "_interpolated";
    TH2F *result = (TH2F*)targetBinning->Clone(histname);
    for (int bx2 = 1; bx2 <= result->GetNbinsX(); ++bx2) {
        for (int by2 = 1; by2 <= result->GetNbinsY(); ++by2) {
            Float_t bin_center_x = result->GetXaxis()->GetBinCenter(bx2);
            Float_t bin_center_y = result->GetYaxis()->GetBinCenter(by2);
            result->SetBinContent(bx2,by2,h->Interpolate(bin_center_x,bin_center_y));
            result->SetBinError(bx2,by2,h_errors->Interpolate(bin_center_x,bin_center_y));
        }
    }
    return result;
}

TH1F *adaptBins(TH1F *h, Int_t nbins, Double_t *axis) {
    TH1F *temp = new TH1F("temp","temp",nbins,axis);
    return adaptBins(h,temp);
}

TH1F *poissonErr(TH1F *h) {
    TString histname = h->GetName();
    histname.Append("_poisson");
    TH1F *result = (TH1F*)h->Clone(histname);
    Double_t entries2integr = h->GetEntries() / h->Integral();
    result->Scale(entries2integr);
    for (int b = 1; b <= result->GetNbinsX(); ++b) {
        result->SetBinError(b,TMath::Sqrt(result->GetBinContent(b)));
    }
    result->Scale(1./entries2integr);
    return result;
}

TH1 *rebinNorm(TH1* orig, Int_t ngroup, const char*newname, const Double_t *xbins)
{
    Int_t nbins    = orig->GetXaxis()->GetNbins();
    Double_t xmin  = orig->GetXaxis()->GetXmin();
    Double_t xmax  = orig->GetXaxis()->GetXmax();
    if ((ngroup <= 0) || (ngroup > nbins)) {
        Error("Rebin", "Illegal value of ngroup=%d",ngroup);
        return 0;
    }

    if (orig->GetDimension() > 1 || orig->InheritsFrom(TProfile::Class())) {
        Error("Rebin", "Operation valid on 1-D histograms only");
        return 0;
    }
    if (!newname && xbins) {
        Error("Rebin","if xbins is specified, newname must be given");
        return 0;
    }

    Int_t newbins = nbins/ngroup;
    if (!xbins) {
        Int_t nbg = nbins/ngroup;
        if (nbg*ngroup != nbins) {
            Warning("Rebin", "ngroup=%d is not an exact divider of nbins=%d.",ngroup,nbins);
        }
    }
    else {
        // in the case that xbins is given (rebinning in variable bins), ngroup is
        // the new number of bins and number of grouped bins is not constant.
        // when looping for setting the contents for the new histogram we
        // need to loop on all bins of original histogram.  Then set ngroup=nbins
        newbins = ngroup;
        ngroup = nbins;
    }

    // Save old bin contents into a new array
    Double_t entries = orig->GetEntries();
    Double_t *oldBins = new Double_t[nbins+2];
    Int_t bin, i;
    for (bin=0;bin<nbins+2;bin++) oldBins[bin] = orig->GetBinContent(bin);
    Double_t *oldErrors = 0;
    if (orig->GetSumw2N() != 0) {
        oldErrors = new Double_t[nbins+2];
        for (bin=0;bin<nbins+2;bin++) oldErrors[bin] = orig->GetBinError(bin);
    }
    // rebin will not include underflow/overflow if new axis range is larger than old axis range
    if (xbins) {
        if (xbins[0] < orig->GetXaxis()->GetXmin() && oldBins[0] != 0 )
            Warning("Rebin","underflow entries will not be used when rebinning");
        if (xbins[newbins] > orig->GetXaxis()->GetXmax() && oldBins[nbins+1] != 0 )
            Warning("Rebin","overflow entries will not be used when rebinning");
    }


    // create a clone of the old histogram if newname is specified
    TH1 *hnew = orig;
    if ((newname && strlen(newname) > 0) || xbins) {
        hnew = (TH1*)orig->Clone(newname);
    }

    // save original statistics
    Double_t stat[13];
    orig->GetStats(stat);
    bool resetStat = false;
    // change axis specs and rebuild bin contents array::RebinAx
    if(!xbins && (newbins*ngroup != nbins)) {
        xmax = orig->GetXaxis()->GetBinUpEdge(newbins*ngroup);
        resetStat = true; //stats must be reset because top bins will be moved to overflow bin
    }
    // save the TAttAxis members (reset by SetBins)
    Int_t    nDivisions  = orig->GetXaxis()->GetNdivisions();
    Color_t  axisColor   = orig->GetXaxis()->GetAxisColor();
    Color_t  labelColor  = orig->GetXaxis()->GetLabelColor();
    Style_t  labelFont   = orig->GetXaxis()->GetLabelFont();
    Float_t  labelOffset = orig->GetXaxis()->GetLabelOffset();
    Float_t  labelSize   = orig->GetXaxis()->GetLabelSize();
    Float_t  tickLength  = orig->GetXaxis()->GetTickLength();
    Float_t  titleOffset = orig->GetXaxis()->GetTitleOffset();
    Float_t  titleSize   = orig->GetXaxis()->GetTitleSize();
    Color_t  titleColor  = orig->GetXaxis()->GetTitleColor();
    Style_t  titleFont   = orig->GetXaxis()->GetTitleFont();

    if(!xbins && (orig->GetXaxis()->GetXbins()->GetSize() > 0)){ // variable bin sizes
        Double_t *bins = new Double_t[newbins+1];
        for(i = 0; i <= newbins; ++i) bins[i] = orig->GetXaxis()->GetBinLowEdge(1+i*ngroup);
        hnew->SetBins(newbins,bins); //this also changes errors array (if any)
        delete [] bins;
    } else if (xbins) {
        hnew->SetBins(newbins,xbins);
    } else {
        hnew->SetBins(newbins,xmin,xmax);
    }

    // Restore axis attributes
    orig->GetXaxis()->SetNdivisions(nDivisions);
    orig->GetXaxis()->SetAxisColor(axisColor);
    orig->GetXaxis()->SetLabelColor(labelColor);
    orig->GetXaxis()->SetLabelFont(labelFont);
    orig->GetXaxis()->SetLabelOffset(labelOffset);
    orig->GetXaxis()->SetLabelSize(labelSize);
    orig->GetXaxis()->SetTickLength(tickLength);
    orig->GetXaxis()->SetTitleOffset(titleOffset);
    orig->GetXaxis()->SetTitleSize(titleSize);
    orig->GetXaxis()->SetTitleColor(titleColor);
    orig->GetXaxis()->SetTitleFont(titleFont);

    // copy merged bin contents (ignore under/overflows)
    // Start merging only once the new lowest edge is reached
    Int_t startbin = 1;
    const Double_t newxmin = hnew->GetXaxis()->GetBinLowEdge(1);
    while( orig->GetXaxis()->GetBinCenter(startbin) < newxmin && startbin <= nbins ) {
        startbin++;
    }
    Int_t oldbin = startbin;
    Double_t binContent, binError;
    for (bin = 1;bin<=newbins;bin++) {
        binContent = 0;
        binError   = 0;
        Int_t imax = ngroup;
        Double_t xbinmax = hnew->GetXaxis()->GetBinUpEdge(bin);
        for (i=0;i<ngroup;i++) {
            if( (oldbin+i > nbins) ||
                    ( hnew != orig && (orig->GetXaxis()->GetBinCenter(oldbin+i) > xbinmax)) ) {
                imax = i;
                break;
            }
            binContent += oldBins[oldbin+i];
            if (oldErrors) binError += oldErrors[oldbin+i]*oldErrors[oldbin+i];
        }
        Double_t binWidth = hnew->GetBinWidth(bin);
        hnew->SetBinContent(bin,binContent/binWidth);
        if (oldErrors) hnew->SetBinError(bin,TMath::Sqrt(binError)/binWidth);
        oldbin += imax;
    }

    // sum underflow and overflow contents until startbin
    binContent = 0;
    binError = 0;
    for (i = 0; i < startbin; ++i)  {
        binContent += oldBins[i];
        if (oldErrors) binError += oldErrors[i]*oldErrors[i];
    }
    hnew->SetBinContent(0,binContent);
    if (oldErrors) hnew->SetBinError(0,TMath::Sqrt(binError));
    // sum overflow
    binContent = 0;
    binError = 0;
    for (i = oldbin; i <= nbins+1; ++i)  {
        binContent += oldBins[i];
        if (oldErrors) binError += oldErrors[i]*oldErrors[i];
    }
    hnew->SetBinContent(newbins+1,binContent);
    if (oldErrors) hnew->SetBinError(newbins+1,TMath::Sqrt(binError));

    // restore statistics and entries modified by SetBinContent
    hnew->SetEntries(entries);
    if (!resetStat) hnew->PutStats(stat);
    delete [] oldBins;
    if (oldErrors) delete [] oldErrors;
    return hnew;
}

TH1F *signalRebin(TH1F *hNP, TH1F *hPP, TH1F *hNN) {
    const Int_t nbins = 26;
    Double_t xAxis1[nbins+1] = {0, 0.010, 0.020, 0.030, 0.040, 0.050, 0.060, 0.070, 0.080, 0.090, 0.110, 0.130, 0.150, 0.170, 0.200, 0.250, 0.300, 0.350, 0.400, 0.450, 0.500, 0.550, 0.600, 0.700, 0.800, 0.900, 1.};
    TH1F *hNP_rebin = (TH1F *)rebinNorm(hNP,nbins,"hNP_rebin",xAxis1);
    TH1F *hPP_rebin = (TH1F *)rebinNorm(hPP,nbins,"hPP_rebin",xAxis1);
    TH1F *hNN_rebin = (TH1F *)rebinNorm(hNN,nbins,"hNN_rebin",xAxis1);
    TH1F *avg = geomAvg(hNN_rebin,hPP_rebin);
    hNP_rebin->Add(avg,-1);
    return hNP_rebin;
}

void setInfNanToZero(TH1* h1) {
    for (int bin_x = 1; bin_x <= h1->GetNbinsX(); ++bin_x) {
        for (int bin_y = 1; bin_y <= h1->GetNbinsY(); ++bin_y) {
            for (int bin_z = 1; bin_z <= h1->GetNbinsZ(); ++bin_z) {
                Float_t content_in  = h1->GetBinContent(bin_x,bin_y,bin_z);
                Float_t error_in  = h1->GetBinError(bin_x,bin_y,bin_z);
                if (!TMath::Finite(content_in)) {
                    h1->SetBinContent(bin_x,bin_y,bin_z,0.);
                    h1->SetBinError(bin_x,bin_y,bin_z,0.);
                }
                if (!TMath::Finite(error_in)) {
                    h1->SetBinContent(bin_x,bin_y,bin_z,0.);
                    h1->SetBinError(bin_x,bin_y,bin_z,0.);
                }
            }
        }
    }
}

Float_t integralPt(TH1F *h, Int_t b1, Int_t b2, Float_t power, Float_t &error, bool width) {
    
    Float_t integral = 0;
    error = 0;

    for (int b = b1; b <= b2; ++b) {
        Float_t bcontent = h->GetBinContent(b);
        Float_t berror = h->GetBinError(b);
        Float_t bwidth = 1;
        if (width) {
            bwidth = h->GetBinWidth(b);
        }
        Float_t bcenter = h->GetBinCenter(b);

        integral += TMath::Power(bcenter, power) * bwidth * bcontent;
        error += TMath::Power(TMath::Power(bcenter, power) * bwidth * berror, 2);
    }
    error = TMath::Sqrt(error);
    return integral;

}

Int_t cmpNbins(TH1F *h1, TH1F* h2) {
    if (h1->GetNbinsX() < h2->GetNbinsX()) return -1;
    else if (h1->GetNbinsX() == h2->GetNbinsX()) return 0;
    else return 1;
}

Bool_t compatibleBinning(TH1F* reference, TH1F *tested) {

    bool foundIncompatible = false;
    for (Int_t b = 1; b <= tested->GetNbinsX(); ++b) {
        if (tested->GetBinLowEdge(b) != reference->GetBinLowEdge(b)) {
            cout << "INCOMPATIBLE BINS: i_tested(" << tested->GetName() << ") = " << b 
                << ", lowEdge_tested = " << tested->GetBinLowEdge(b) << ", lowEdge_reference(" << reference->GetName() << ") = " << reference->GetBinLowEdge(b) << endl;
            foundIncompatible = true;
        }
    }
    return !foundIncompatible;

}

void addCompatible(TH1F *h1, TH1F* h2, Float_t scale = 1.0) {
    TH1F *hA, *hB;
    if (compatibleBinning(h1, h2)) {
        hA = h1;
        hB = (TH1F*)h2->Clone("hB");
        hB->Scale(scale);
    }
    else if (compatibleBinning(h2, h1)) {
        hB = h1;
        hA = (TH1F*)h2->Clone("hB");
        hA->Scale(scale);
    }
    else return;

    for (Int_t b = 1; b <= hB->GetNbinsX(); ++b) {

        hA->SetBinContent(b, hA->GetBinContent(b) + hB->GetBinContent(b));
        Float_t errA2 = TMath::Power(hA->GetBinError(b), 2);
        Float_t errB2 = TMath::Power(hB->GetBinError(b), 2);
        Float_t error = TMath::Sqrt(errA2 + errB2);
        hA->SetBinError(b, error);
    }
}

#endif

#include "hrpchit.h"
#include "hdileptonanalysis.h"
#include "hcategorymanager.h"
#include "hcategory.h"
#include "hparticletool.h"
#include "hparticleanglecor.h"

static const int ALL_MULTS = 0;
static const int NO_CUT = 0;
static const int CUT_INCOMP = 1;
static const int CUT_INCOMP_NEARST = 2;
static const int CUT_NEARST = 3;

using namespace TMath;

TH1F *h_dtheta_theta_mean[6];
TH1F *h_dtheta_theta_sigma[6];
TH1F *h_dphi_theta_mean[6];
TH1F *h_dphi_theta_sigma[6];
TH1F *h_dtheta_mom_mean[6];
TH1F *h_dtheta_mom_sigma[6];
TH1F *h_dphi_mom_mean[6];
TH1F *h_dphi_mom_sigma[6];

std::vector<HDileptonAnalysis::ParticleEmission> doTrackCorr(HParticleAngleCor &trackcor, HCategory *candCat)
{
    std::vector<HDileptonAnalysis::ParticleEmission> emissionBefore;
    Int_t size = candCat->getEntries();
    for (Int_t j = 0; j < size; j++)
    {
        HParticleCand *cand = NULL;
        cand = HCategoryManager::getObject(cand, candCat, j);
        emissionBefore.push_back(HDileptonAnalysis::ParticleEmission(
            cand->getTheta(),
            cand->getPhi(),
            cand->getR(),
            cand->getZ(),
            cand->getRichMatchingQuality()));
        trackcor.recalcSetEmission(cand); // changes values inside candidate
                                          //        trackcor.realignRichRing(cand); //
    }
    return emissionBefore;
}

// something that can be indexed and contains floats
template <typename T>
Int_t findNearestNeighbor(HParticleCand *cand1, T &angleNearestBoth, T &angleNearestSeg, T &angleNearestFull, HCategory *candCat)
{
    Int_t nearestIndex = -1;
    Int_t j = cand1->getIndex();
    Int_t size = candCat->getEntries();
    angleNearestBoth[j] = -999;
    angleNearestSeg[j] = -999;
    angleNearestFull[j] = -999;
    HParticleCand *cand2;

    for (Int_t k = 0; k < size; k++)
    { // second candidate, k > j condition applied below
        if (k == j)
            continue;
        cand2 = HCategoryManager::getObject(cand2, candCat, k);
        Float_t mdcdEdx2 = cand2->getMdcdEdx();
        Bool_t notLeptonlike = mdcdEdx2 > 3;

        if (!cand2->isFakeRejected(-1) /* && cand2->getChi2() > 100 */ /* && !(cand1->getMetaHitInd() == cand2->getMetaHitInd() && cand1->getChi2() < cand2->getChi2())*/ && cand2->getChi2() != cand1->getChi2() /* && cand2->getOuterSegInd() == -1*/)
        {
            Float_t newAngleNearest = HParticleTool::getOpeningAngle(cand1->getPhi(), cand1->getTheta(), cand2->getPhi(), cand2->getTheta());

            if (angleNearestSeg[j] == -999 || fabs(newAngleNearest) < fabs(angleNearestSeg[j]))
            {
                angleNearestSeg[j] = newAngleNearest;

                if (cand2->getRichInd() == -1 || notLeptonlike)
                {
                    angleNearestSeg[j] *= -1;
                }
                nearestIndex = k;
            }
        }
    }
    return nearestIndex;
}

/*
const int NSIGNS = 3;
const int NMULTS = 3; // all, mult. trigger, min. bias
const int NCUTS = 1;
TString signs[NSIGNS] = { "NP", "PP", "NN" };
TString mults[NMULTS] = { "", "_multTrig", "_minBias" };
TString cuts[NCUTS] = { "" };
*/

void fillPair(HHistMap &hM, const char *pattern, HParticleCand *cand1, HParticleCand *cand2, Float_t x, Float_t y, Float_t weight, bool likeSignOnly = kFALSE, bool dontCorrectNP = kFALSE)
{
    char histname[128];
    if ((1 == cand1->getCharge() && -1 == cand2->getCharge()) ||
        (1 == cand2->getCharge() && -1 == cand1->getCharge()))
    {
        if (likeSignOnly)
        {
            return;
        }
        else
        {
            sprintf(histname, pattern, "NP");
        }
        if (dontCorrectNP)
            weight = 1;
    }
    if (1 == cand1->getCharge() && 1 == cand2->getCharge())
    {
        sprintf(histname, pattern, "PP");
    }
    if (-1 == cand1->getCharge() && -1 == cand2->getCharge())
    {
        sprintf(histname, pattern, "NN");
    }
    //cout << histname << endl;
    //return;
    if (hM.get(histname) != NULL)
    {
        TString strHistname(histname);
        if (!strHistname.Contains("Fine") && strHistname.Contains("mass"))
        {
            Int_t foundBinX = hM.get(histname)->GetXaxis()->FindBin(x);
            Float_t binWidthX = hM.get(histname)->GetXaxis()->GetBinWidth(foundBinX);
            Int_t foundBinY = hM.get(histname)->GetYaxis()->FindBin(y);
            Float_t binWidthY = hM.get(histname)->GetYaxis()->GetBinWidth(foundBinY);
            Float_t binArea = binWidthX * binWidthY;
            hM.get2(histname)->Fill(x, y, weight / binArea);
        }
        else
        {
            hM.get2(histname)->Fill(x, y, weight);
        }
    }
    else
    {
        //printf("fillPair(... , Float x, Float y, ...): histogram %s not found\n",histname);
    }
}

void fillPair(HHistMap &hM, const char *pattern, HParticleCand *cand1, HParticleCand *cand2, Float_t value, Float_t weight = 1., bool likeSignOnly = kFALSE)
{
    char histname[128];
    //cout << cand1->getCharge() << " " << cand2->getCharge() << endl;
    if ((1 == cand1->getCharge() && -1 == cand2->getCharge()) ||
        (1 == cand2->getCharge() && -1 == cand1->getCharge()))
    {
        //cout << "should be NP..." << endl;
        if (likeSignOnly)
        {
            return;
        }
        else
        {
            sprintf(histname, pattern, "NP");
        }
    }
    if (1 == cand1->getCharge() && 1 == cand2->getCharge())
    {
        //cout << "should be PP..." << endl;
        sprintf(histname, pattern, "PP");
    }
    if (-1 == cand1->getCharge() && -1 == cand2->getCharge())
    {
        //cout << "should be NN..." << endl;
        sprintf(histname, pattern, "NN");
    }
    //cout << pattern << endl;
    //cout << histname << endl;
    //return;
    if (hM.get(histname) != NULL)
    {
        TString strHistname(histname);
        if (strHistname.Contains("hmass") && !strHistname.Contains("Fine") && !strHistname.Contains("hmassy") && !strHistname.Contains("hmasspt"))
        {
            Int_t foundBin = hM.get(histname)->FindBin(value);
            Float_t binWidth = hM.get(histname)->GetBinWidth(foundBin);
            hM.get(histname)->Fill(value, weight / binWidth);
        }
        else
        {
            hM.get(histname)->Fill(value, weight);
        }
    }
    else
    {
        //printf("fillPair(... , Float value, ...): histogram %s not found\n",histname);
    }
}

void fillPairSpectra(HHistMap &hM, 
    HParticleCand *cand1, HParticleCand *cand2,
    HDileptonAnalysis::ParticleEmission emission1, HDileptonAnalysis::ParticleEmission emission2,
    TString suffix, Int_t mult_bin, Int_t cut, Float_t eventWeight = 1.)
{
    HVertex PrimVertexReco = ((HEventHeader *)(gHades->getCurrentEvent()->getHeader()))->getVertexReco();
    TLorentzVector dilep = (*cand1) + (*cand2);

    TLorentzVector dilep_cm(dilep);
    dilep_cm.Boost(0., 0., -0.629); // go to NN CM system for 1230MeV

    Float_t oAngle = cand1->Angle(cand2->Vect()) * RadToDeg();
    Float_t pairW = dilep_cm.E();
    Float_t pairP = dilep_cm.Vect().Mag();
    Float_t invM = dilep.M();
    Float_t omega = dilep.E();
    Float_t pt = dilep.Perp();
    Float_t rapidity = dilep.Rapidity();

    HGeomVector base1, dir1;
    HParticleTool::calcSegVector(emission1.z, emission1.r, emission1.phi, emission1.theta, base1, dir1);
    HGeomVector base2, dir2;
    HParticleTool::calcSegVector(emission2.z, emission2.r, emission2.phi, emission2.theta, base2, dir2);
    HGeomVector verAn = HParticleTool::calcVertexAnalytical(base1, dir1, base2, dir2);
    Float_t verAnX = verAn.getX();
    Float_t verAnY = verAn.getY();
    Float_t verAnZ = verAn.getZ();
    Float_t verAnR = TMath::Sqrt(verAnX * verAnX + verAnY * verAnY);

    if (!suffix.Contains("Sorter"))
    {
        if (mult_bin < HDileptonAnalysis::NMULTS)
        {
            suffix = HDileptonAnalysis::mults[mult_bin] + HDileptonAnalysis::cuts[cut] + suffix;
        }
    }

    fillPair(hM, TString("hverZR%s") + suffix, cand1, cand2, verAnZ, verAnR, eventWeight, false, false);
    fillPair(hM, TString("hp1p2%s") + suffix, cand1, cand2, cand1->getMomentum(), cand2->getMomentum(), eventWeight, false, false);
    fillPair(hM, TString("hpty%s") + suffix, cand1, cand2, pt, rapidity, eventWeight, false, false);
    fillPair(hM, TString("henergymom%s") + suffix, cand1, cand2, pairW, pairP, eventWeight, false, false);
    fillPair(hM, TString("hmasspt%s") + suffix, cand1, cand2, invM / 1000, pt, eventWeight, false, false);
    fillPair(hM, TString("hmass%s") + suffix, cand1, cand2, invM / 1000, eventWeight);
    fillPair(hM, TString("hw%s") + suffix, cand1, cand2, omega / 1000, eventWeight);
    fillPair(hM, TString("hoAngle%s") + suffix, cand1, cand2, oAngle, eventWeight);
    //---

    if (invM < 150)
    {
        fillPair(hM, TString("hy0to150%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt0to150%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 150 && invM < 450)
    {
        fillPair(hM, TString("hy150to450%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt150to450%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 450 && invM < 700)
    {
        fillPair(hM, TString("hy450to700%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt450to700%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 700 && invM < 1020)
    {
        fillPair(hM, TString("hy700to1020%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt700to1020%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 1020 && invM < 2000)
    {
        fillPair(hM, TString("hy1020to2000%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt1020to2000%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 150 && invM < 225)
    {
        fillPair(hM, TString("hy150to225%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt150to225%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 225 && invM < 300)
    {
        fillPair(hM, TString("hy225to300%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt225to300%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 300 && invM < 375)
    {
        fillPair(hM, TString("hy300to375%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt300to375%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 375 && invM < 450)
    {
        fillPair(hM, TString("hy375to450%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt375to450%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 450 && invM < 600)
    {
        fillPair(hM, TString("hy450to600%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt450to600%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (invM >= 600 && invM < 1000)
    {
        fillPair(hM, TString("hy600to1000%s") + suffix, cand1, cand2, rapidity, eventWeight);
        fillPair(hM, TString("hpt600to1000%s") + suffix, cand1, cand2, pt, eventWeight);
    }
    if (pt < 200)
    {
        fillPair(hM, TString("hmassPt0to200%s") + suffix, cand1, cand2, invM / 1000, eventWeight);
    }
    if (pt > 200 && pt < 400)
    {
        fillPair(hM, TString("hmassPt200to400%s") + suffix, cand1, cand2, invM / 1000, eventWeight);
    }
    if (pt > 400 && pt < 1000)
    {
        fillPair(hM, TString("hmassPt400to1000%s") + suffix, cand1, cand2, invM / 1000, eventWeight);
    }
}

void addPairHists(HHistMap &hMPair)
{
    // Reference
    Int_t nbins2 = 39;
    Double_t xAxis2[] = {0, 0.005, 0.01, 0.015, 0.02, 0.025, 0.03, 0.035, 0.04, 0.045, 0.05, 0.055, 0.06, 0.065, 0.07, 0.075, 0.08, 0.09, 0.1, 0.12, 0.16, 0.2, 0.24, 0.28, 0.32, 0.36, 0.4, 0.44, 0.48, 0.52, 0.56, 0.6, 0.64, 0.68, 0.72, 0.76, 0.8, 0.9, 1, 1.1};
    HDileptonAnalysis::addHists(hMPair, "henergymom", HDileptonAnalysis::binning(120, 0, 1200), HDileptonAnalysis::binning(120, 0, 1200), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hmasspt", HDileptonAnalysis::binning(nbins2, xAxis2), HDileptonAnalysis::binning(120, 0, 1200), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hverZR", HDileptonAnalysis::binning(400, -200, 100), HDileptonAnalysis::binning(400, 0, 100), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hp1p2", HDileptonAnalysis::binning(200, 0, 2000), HDileptonAnalysis::binning(200, 0, 2000), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpty", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::binning(120, 0, 1200), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hw", HDileptonAnalysis::binning(800, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hmass", HDileptonAnalysis::binning(800, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hmassPt0to200", HDileptonAnalysis::binning(800, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hmassPt200to400", HDileptonAnalysis::binning(800, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hmassPt400to1000", HDileptonAnalysis::binning(800, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy0to150", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy150to450", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy450to700", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy700to1020", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy1020to2000", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy150to225", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy225to300", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy300to375", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy375to450", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy450to600", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hy600to1000", HDileptonAnalysis::binning(100, 0, 2), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt0to150", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt150to450", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt450to700", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt700to1020", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt1020to2000", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt150to225", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt225to300", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt300to375", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt375to450", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt450to600", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hpt600to1000", HDileptonAnalysis::binning(100, 0, 1000), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
    HDileptonAnalysis::addHists(hMPair, "hoAngle", HDileptonAnalysis::binning(200, 0, 200), HDileptonAnalysis::null_binning(), HDileptonAnalysis::null_binning());
}

void readRichMatchingParams(TString filename)
{
    TFile *f = new TFile(filename, "READ");
    for (int sec = 0; sec < 6; ++sec)
    {
        h_dtheta_theta_mean[sec] = (TH1F *)f->Get(Form("hfits_mean_exp_sec%i_dtheta_theta", sec));
        h_dtheta_theta_sigma[sec] = (TH1F *)f->Get(Form("hfits_sigma_exp_sec%i_dtheta_theta", sec));
        h_dphi_theta_mean[sec] = (TH1F *)f->Get(Form("hfits_mean_exp_sec%i_dphi_theta", sec));
        h_dphi_theta_sigma[sec] = (TH1F *)f->Get(Form("hfits_sigma_exp_sec%i_dphi_theta", sec));
        h_dtheta_mom_mean[sec] = (TH1F *)f->Get(Form("hfits_mean_exp_sec%i_dtheta_mom", sec));
        h_dtheta_mom_sigma[sec] = (TH1F *)f->Get(Form("hfits_sigma_exp_sec%i_dtheta_mom", sec));
        h_dphi_mom_mean[sec] = (TH1F *)f->Get(Form("hfits_mean_exp_sec%i_dphi_mom", sec));
        h_dphi_mom_sigma[sec] = (TH1F *)f->Get(Form("hfits_sigma_exp_sec%i_dphi_mom", sec));
    }
}

Bool_t isRichQaInSigmaTheta(HParticleCand *cand, Int_t nsigma)
{
    Int_t sec = cand->getSector();
    Float_t theta = cand->getTheta();

    Float_t dtheta_mean = h_dtheta_theta_mean[sec]->Interpolate(theta);
    Float_t dtheta_sigma = h_dtheta_theta_sigma[sec]->Interpolate(theta);
    Float_t dphi_mean = h_dphi_theta_mean[sec]->Interpolate(theta);
    Float_t dphi_sigma = h_dphi_theta_sigma[sec]->Interpolate(theta);

    Float_t dtheta = cand->getDeltaTheta();
    Float_t dphi = cand->getDeltaPhi();

    Float_t nsigma_dtheta = (dtheta - dtheta_mean) / dtheta_sigma;
    Float_t nsigma_dphi = (dphi - dphi_mean) / dphi_sigma;

    Float_t nsigma_r = TMath::Sqrt(TMath::Power(nsigma_dtheta, 2) + TMath::Power(nsigma_dphi, 2));

    return nsigma_r < nsigma;
}

Bool_t isRichQaInSigmaMom(HParticleCand *cand, Int_t nsigma)
{
    Int_t sec = cand->getSector();
    Float_t mom = cand->getMomentum();

    Float_t dtheta_mean = h_dtheta_mom_mean[sec]->Interpolate(mom);
    Float_t dtheta_sigma = h_dtheta_mom_sigma[sec]->Interpolate(mom);
    Float_t dphi_mean = h_dphi_mom_mean[sec]->Interpolate(mom);
    Float_t dphi_sigma = h_dphi_mom_sigma[sec]->Interpolate(mom);

    Float_t dtheta = cand->getDeltaTheta();
    Float_t dphi = cand->getDeltaPhi();

    Float_t nsigma_dtheta = (dtheta - dtheta_mean) / dtheta_sigma;
    Float_t nsigma_dphi = (dphi - dphi_mean) / dphi_sigma;

    Float_t nsigma_r = TMath::Sqrt(TMath::Power(nsigma_dtheta, 2) + TMath::Power(nsigma_dphi, 2));

    return nsigma_r < nsigma;
}

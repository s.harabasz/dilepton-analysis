#ifndef H_DILEPTON_ANALYSIS
#define H_DILEPTON_ANALYSIS

#include <iostream>
#include <utility>
#include <memory>
#include <type_traits>

#include "hparticlecand.h"
#include "hhistmap.h"
#include "TMath.h"

namespace HDileptonAnalysis
{
    struct ParticleEmission
    {
        ParticleEmission(Float_t _theta, Float_t _phi, Float_t _r, Float_t _z, Float_t _richQa)
            : theta(_theta), phi(_phi), r(_r), z(_z), richQa(_richQa) {}
        Float_t theta;
        Float_t phi;
        Float_t r;
        Float_t z;
        Float_t richQa;
    };

    template <typename T>
    using FncValue = float (*)(const T *, const ParticleEmission);

    class IBinning
    {
    public:
        virtual Int_t nbins() const = 0;
        virtual Double_t *bins() const = 0;
    };
    class IBinningNull : public IBinning
    {
    };
    class IBinningNotNull : public IBinning
    {
    };
    class BinningArray : public IBinningNotNull
    {
    public:
        BinningArray(Int_t _nbins, Double_t *_bins) : m_nbins(_nbins), m_bins(_bins) {}
        Int_t nbins() const override { return m_nbins; }
        Double_t *bins() const override { return m_bins; }

    private:
        Int_t m_nbins;
        Double_t *m_bins;
    };
    class BinningNullArray : public IBinningNull
    {
    public:
        Int_t nbins() const override { return 0; }
        Double_t *bins() const override { return nullptr; }
    };
    TString getHistType(std::shared_ptr<BinningArray>, std::shared_ptr<BinningArray>, std::shared_ptr<BinningArray>) { return "TH3F"; }
    TString getHistType(std::shared_ptr<BinningArray>, std::shared_ptr<BinningArray>, std::shared_ptr<BinningNullArray>) { return "TH2F"; }
    TString getHistType(std::shared_ptr<BinningArray>, std::shared_ptr<BinningNullArray>, std::shared_ptr<BinningNullArray>) { return "TH1F"; }

    void addHistToMap(HHistMap &hM, TString type, TString name, TString title,
                      std::shared_ptr<IBinning> binsX, std::shared_ptr<IBinning> binsY, std::shared_ptr<IBinning> binsZ)
    {
        hM.addHistN(type, name, title, binsX->nbins(), binsX->bins(), binsY->nbins(), binsY->bins(), binsZ->nbins(), binsZ->bins());
    }

    std::shared_ptr<BinningArray> binning(Int_t nbins, Float_t min, Float_t max)
    {
        Double_t *bins = new Double_t[nbins + 1];
        Float_t dx = (max - min) / nbins;
        bins[0] = min;
        for (int i = 0; i < nbins; ++i)
        {
            bins[i + 1] = min + (i + 1) * dx;
        }
        return std::shared_ptr<BinningArray>(new BinningArray(nbins, bins));
    }
    std::shared_ptr<BinningArray> binning(Int_t nbins, Double_t *bins)
    {
        return std::shared_ptr<BinningArray>(new BinningArray(nbins, bins));
    }
    std::shared_ptr<BinningNullArray> null_binning()
    {
        return std::shared_ptr<BinningNullArray>(new BinningNullArray());
    }

    const int NSIGNS = 3;
    const int NMULTS = 4; // all, mult. trigger, min. bias, other tBit
    const int NCUTS = 1;
    TString signs[NSIGNS] = {"NP", "PP", "NN"};
    TString mults[NMULTS] = {"", "_multTrig", "_minBias", "_otherTBit"};
    TString cuts[NCUTS] = {""};

    void addHists(HHistMap &hM, TString type, TString prefix, std::shared_ptr<IBinning> binningx, std::shared_ptr<IBinning> binningy, std::shared_ptr<IBinning> binningz)
    {
        for (int i = 0; i < NSIGNS; ++i)
        {
            for (int b = 0; b < NMULTS; ++b)
            {
                for (int c = 0; c < NCUTS; ++c)
                {
                    addHistToMap(hM, type, prefix + signs[i] + mults[b] + cuts[c], prefix + signs[i] + mults[b] + cuts[c],
                                 binningx, binningy, binningz);
                    addHistToMap(hM, type, prefix + signs[i] + mults[b] + cuts[c] + "_oa9deg", prefix + signs[i] + mults[b] + cuts[c] + "_oa9deg",
                                 binningx, binningy, binningz);
                    addHistToMap(hM, type, prefix + signs[i] + mults[b] + cuts[c] + "_oa9degRecur", prefix + signs[i] + mults[b] + cuts[c] + "_oa9degRecur",
                                 binningx, binningy, binningz);
                    addHistToMap(hM, type, prefix + signs[i] + mults[b] + cuts[c] + "_oaRingNotConversion", prefix + signs[i] + mults[b] + cuts[c] + "_oaRingNotConversion",
                                 binningx, binningy, binningz);
                    addHistToMap(hM, type, prefix + signs[i] + mults[b] + cuts[c] + "_oaRingNotConversion_abovePi0", prefix + signs[i] + mults[b] + cuts[c] + "_oaRingNotConversion",
                                 binningx, binningy, binningz);
                }
                addHistToMap(hM, type, prefix + signs[i] + "_mix" + mults[b], prefix + signs[i] + "_mix" + mults[b],
                             binningx, binningy, binningz);
                addHistToMap(hM, type, prefix + signs[i] + "_mix" + mults[b] + "_oa9deg", prefix + signs[i] + "_mix" + mults[b] + "_oa9deg",
                             binningx, binningy, binningz);
            }
        }
    }
    template <typename TBinningX, typename TBinningY, typename TBinningZ>
    void addHists(HHistMap &hM, TString prefix, std::shared_ptr<TBinningX> binningx, std::shared_ptr<TBinningY> binningy, std::shared_ptr<TBinningZ> binningz)
    {
        TString type = getHistType(binningx, binningy, binningz);
        addHists(hM, type, prefix, binningx, binningy, binningz);
    }

    class HistNameFormatter
    {
    public:
        virtual TString operator()(TString histname, const char *whatkind, const HParticleCand *) const = 0;
        virtual TString getFormat() const { return format; }

    protected:
        HistNameFormatter(TString _format) : format(_format) {}
        TString format;
    };
    class HistKindSysSecFormatter : public HistNameFormatter
    {
    public:
        HistKindSysSecFormatter() : HistNameFormatter("%s%s_sys%i_sec%i") {}
        TString operator()(TString histname, const char *whatkind, const HParticleCand *cand) const override
        {
            return Form(format.Data(), histname.Data(), whatkind, cand->getSystemUsed(), cand->getSector());
        }
    };
    class HistKindSysFormatter : public HistNameFormatter
    {
    public:
        HistKindSysFormatter() : HistNameFormatter("%s%s_sys%i") {}
        TString operator()(TString histname, const char *whatkind, const HParticleCand *cand) const override
        {
            return Form(format.Data(), histname.Data(), whatkind, cand->getSystemUsed());
        }
    };

    class IHistCreator
    {
    public:
        virtual void createHists(HHistMap &, std::shared_ptr<HistNameFormatter>,
                                 TString histname, const char *whatkind, std::vector<std::shared_ptr<BinningArray>> binnings) = 0;
    };
    class Hist1DSysSecCreator : public IHistCreator
    {
    public:
        void createHists(HHistMap &hM, std::shared_ptr<HistNameFormatter> formatter,
                         TString histname, const char *whatkind, std::vector<std::shared_ptr<BinningArray>> binnings) override
        {

            for (int sys = 0; sys < 2; ++sys)
            {
                for (int sec = 0; sec < 6; ++sec)
                {
                    TString nametitle = Form(formatter->getFormat().Data(), histname.Data(), whatkind, sys, sec);
                    hM.addHistN("TH1F", nametitle, nametitle,
                                binnings.at(0)->nbins(), binnings.at(0)->bins());
                }
            }
        }
    };
    class Hist1DSysCreator : public IHistCreator
    {
    public:
        void createHists(HHistMap &hM, std::shared_ptr<HistNameFormatter> formatter,
                         TString histname, const char *whatkind, std::vector<std::shared_ptr<BinningArray>> binnings) override
        {

            for (int sys = 0; sys < 2; ++sys)
            {
                TString nametitle = Form(formatter->getFormat().Data(), histname.Data(), whatkind, sys);
                hM.addHistN("TH1F", nametitle, nametitle,
                            binnings[0]->nbins(), binnings[0]->bins());
            }
        }
    };
    class Hist2DSysSecCreator : public IHistCreator
    {
    public:
        void createHists(HHistMap &hM, std::shared_ptr<HistNameFormatter> formatter,
                         TString histname, const char *whatkind, std::vector<std::shared_ptr<BinningArray>> binnings) override
        {

            for (int sys = 0; sys < 2; ++sys)
            {
                for (int sec = 0; sec < 6; ++sec)
                {
                    TString nametitle = Form(formatter->getFormat().Data(), histname.Data(), whatkind, sys, sec);
                    hM.addHistN("TH2F", nametitle, nametitle,
                                binnings.at(0)->nbins(), binnings.at(0)->bins(), binnings.at(1)->nbins(), binnings.at(1)->bins());
                }
            }
        }
    };
    class Hist2DSysCreator : public IHistCreator
    {
    public:
        void createHists(HHistMap &hM, std::shared_ptr<HistNameFormatter> formatter,
                         TString histname, const char *whatkind, std::vector<std::shared_ptr<BinningArray>> binnings) override
        {

            for (int sys = 0; sys < 2; ++sys)
            {
                TString nametitle = Form(formatter->getFormat().Data(), histname.Data(), whatkind, sys);
                hM.addHistN("TH2F", nametitle, nametitle,
                            binnings.at(0)->nbins(), binnings.at(0)->bins(), binnings.at(1)->nbins(), binnings.at(1)->bins());
            }
        }
    };
    class Hist3DSysSecCreator : public IHistCreator
    {
    public:
        void createHists(HHistMap &hM, std::shared_ptr<HistNameFormatter> formatter,
                         TString histname, const char *whatkind, std::vector<std::shared_ptr<BinningArray>> binnings) override
        {

            for (int sys = 0; sys < 2; ++sys)
            {
                for (int sec = 0; sec < 6; ++sec)
                {
                    TString nametitle = Form(formatter->getFormat().Data(), histname.Data(), whatkind, sys, sec);
                    hM.addHistN("TH3F", nametitle, nametitle,
                                binnings.at(0)->nbins(), binnings.at(0)->bins(), binnings.at(1)->nbins(), binnings.at(1)->bins());
                }
            }
        }
    };

    class IHist
    {
    public:
        virtual void fill(const HParticleCand *, const ParticleEmission, const char *whatkind) = 0;
        virtual TString getName() = 0;
    };
    template <typename T>
    class SingleHist1D : public IHist
    {
    public:
        SingleHist1D(
            HHistMap &_hM, TString _histname, const char *whatkind, std::shared_ptr<HistNameFormatter> _formatter, IHistCreator &creator,
            FncValue<T> _fncValueX, std::vector<std::shared_ptr<BinningArray>> binnings,
            bool (*_pred)(const T *) = [](const T *) -> bool
            { return true; })
            : hM(_hM), histname(_histname), formatter(_formatter), fncValueX(_fncValueX), pred(_pred)
        {
            creator.createHists(hM, formatter, histname, whatkind, binnings);
        }
        void fill(const T *cand, const ParticleEmission emission, const char *whatkind) override
        {
            if (pred(cand))
            {
                hM.get((*formatter)(histname, whatkind, cand))->Fill(fncValueX(cand, emission));
            }
        }
        TString getName() override
        {
            return histname + " " + formatter->getFormat();
        }

    protected:
        HHistMap &hM;
        TString histname;
        std::shared_ptr<HistNameFormatter> formatter;
        FncValue<T> fncValueX;
        bool (*pred)(const T *);
    };

    template <typename T>
    class SingleHist2D : public SingleHist1D<T>
    {
    public:
        SingleHist2D(
            HHistMap &_hM, TString _histname, const char *whatkind, std::shared_ptr<HistNameFormatter> _formatter, IHistCreator &creator,
            FncValue<T> _fncValueX, FncValue<T> _fncValueY, std::vector<std::shared_ptr<BinningArray>> binnings,
            bool (*_pred)(const T *) = [](const T *) -> bool
            { return true; })
            : SingleHist1D<T>(_hM, _histname, whatkind, _formatter, creator, _fncValueX, binnings, _pred), fncValueY(_fncValueY)
        {
        }
        void fill(const T *cand, const ParticleEmission emission, const char *whatkind) override
        {
            if (this->pred(cand))
            {
                this->hM.get2((*this->formatter)(this->histname, whatkind, cand))->Fill(this->fncValueX(cand, emission), this->fncValueY(cand, emission));
            }
        }

    protected:
        FncValue<T> fncValueY;
    };

    template <typename T>
    class SingleHist3D : public SingleHist2D<T>
    {
    public:
        SingleHist3D(
            HHistMap &_hM, TString _histname, const char *whatkind, std::shared_ptr<HistNameFormatter> _formatter, IHistCreator &creator,
            FncValue<T> _fncValueX, FncValue<T> _fncValueY, FncValue<T> _fncValueZ, std::vector<std::shared_ptr<BinningArray>> binnings,
            bool (*_pred)(const T *) = [](const T *) -> bool
            { return true; })
            : SingleHist2D<T>(_hM, _histname, whatkind, _formatter, creator, _fncValueX, _fncValueY, binnings, _pred), fncValueZ(_fncValueZ)
        {
        }
        void fill(const T *cand, const ParticleEmission emission, const char *whatkind) override
        {
            if (this->pred(cand))
            {
                this->hM.get3((*this->formatter)(this->histname, whatkind, cand))->Fill(this->fncValueX(cand, emission), this->fncValueY(cand, emission), this->fncValueZ(cand, emission));
            }
        }

    protected:
        FncValue<T> fncValueZ;
    };

    class Analysis
    {
    private:
        std::map<const char *, std::vector<std::shared_ptr<IHist>>> hists;

    public:
        void addSingleHistsMinimal(HHistMap &hM, const char *whatkind)
        {
            std::shared_ptr<HistKindSysFormatter> histKindSysFormatter = std::shared_ptr<HistKindSysFormatter>(new HistKindSysFormatter);
            std::shared_ptr<HistKindSysSecFormatter> histKindSysSecFormatter = std::shared_ptr<HistKindSysSecFormatter>(new HistKindSysSecFormatter);
            Hist2DSysSecCreator hist2DSysSecCreator;
            Hist1DSysSecCreator hist1DSysSecCreator;
            std::vector<std::shared_ptr<BinningArray>> binnings = {binning(180, 0, 360), binning(45, 0, 90)};

            auto FncMomCharge = [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getMomentum() * c->getCharge(); };

            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "BetaMom", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getBeta(); },
                {binning(250, -2000, 3000), binning(200, 0, 1.5)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist1D<HParticleCand>(hM, "Mom", whatkind, histKindSysSecFormatter, hist1DSysSecCreator,
                                                                                             FncMomCharge,
                                                                                             {binning(250, -2000, 3000)})));
        }
        void addSingleHistsFull(HHistMap &hM, const char *whatkind)
        {

            std::shared_ptr<HistKindSysFormatter> histKindSysFormatter = std::shared_ptr<HistKindSysFormatter>(new HistKindSysFormatter);
            std::shared_ptr<HistKindSysSecFormatter> histKindSysSecFormatter = std::shared_ptr<HistKindSysSecFormatter>(new HistKindSysSecFormatter);
            Hist2DSysCreator hist2DSysCreator;
            Hist1DSysCreator hist1DSysCreator;
            Hist3DSysSecCreator hist3DSysSecCreator;
            Hist2DSysSecCreator hist2DSysSecCreator;
            Hist1DSysSecCreator hist1DSysSecCreator;
            std::vector<std::shared_ptr<BinningArray>> binnings = {binning(180, 0, 360), binning(45, 0, 90)};

            auto FncMomCharge = [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getMomentum() * c->getCharge(); };

            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "ThetaPhi", whatkind, histKindSysFormatter, hist2DSysCreator,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getPhi(); },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getTheta(); },
                {binning(180, 0, 360), binning(45, 0, 90)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "TofMom", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getTof(); },
                {binning(250, -2000, 3000), binning(100, 0, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "BetaMom", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getBeta(); },
                {binning(250, -2000, 3000), binning(100, 0, 1.5)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "DthetaDphi", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaPhi(); },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaTheta(); },
                {binning(200, -10, 10), binning(100, -10, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "DthetaTheta", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getTheta(); },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaTheta(); },
                {binning(90, 0, 90), binning(100, -10, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaMom", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(300, -1200, 1200), binning(100, 0, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "DphiMom", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaPhi(); },
                {binning(300, -1200, 1200), binning(100, -10, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "DthetaMom", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaTheta(); },
                {binning(300, -1200, 1200), binning(100, -10, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "DphiTheta", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getTheta(); },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaPhi(); },
                {binning(45, 0, 90), binning(100, -10, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist3D<HParticleCand>(
                hM, "DphiDthetaTheta", whatkind, histKindSysSecFormatter, hist3DSysSecCreator,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaPhi(); },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getDeltaTheta(); },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getTheta(); },
                {binning(100, -10, 10), binning(100, -10, 10), binning(90, 0, 90)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "MassMom", whatkind, histKindSysSecFormatter, hist2DSysSecCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getMass(); },
                {binning(250, -2000, 3000), binning(120, 0, 1200)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist1D<HParticleCand>(hM, "Mom", whatkind, histKindSysSecFormatter, hist1DSysSecCreator,
                                                                                             FncMomCharge,
                                                                                             {binning(250, -2000, 3000)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist1D<HParticleCand>(hM, "Mom", whatkind, histKindSysFormatter, hist1DSysCreator,
                                                                                             FncMomCharge,
                                                                                             {binning(250, -2000, 3000)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist1D<HParticleCand>(hM, "Phi", whatkind, histKindSysFormatter, hist1DSysCreator,
                                                                                             [](const HParticleCand *c, const ParticleEmission) -> float
                                                                                             { return c->getPhi(); },
                                                                                             {binning(180, 0, 360)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist1D<HParticleCand>(hM, "Mass", whatkind, histKindSysSecFormatter, hist1DSysSecCreator,
                                                                                             FncMomCharge,
                                                                                             {binning(250, -2000, 3000)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist1D<HParticleCand>(hM, "Mass", whatkind, histKindSysFormatter, hist1DSysCreator,
                                                                                             [](const HParticleCand *c, const ParticleEmission) -> float
                                                                                             { return c->getSector(); },
                                                                                             {binning(6, 0, 6)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "MassMom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getMass(); },
                {binning(250, -2000, 3000), binning(120, 0, 1200)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "BetaMom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getBeta(); },
                {binning(250, -2000, 3000), binning(100, 0, 1.5)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "MdcdEdxMom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getMdcdEdx(); },
                {binning(250, -2000, 3000), binning(100, 0, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "ThetaMom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getTheta(); },
                {binning(250, -2000, 3000), binning(45, 0, 90)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "BetaRingRadius", whatkind, histKindSysFormatter, hist2DSysCreator,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getRingRadius(); },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getBeta(); },
                {binning(70, 0, 35), binning(100, 0, 1.5)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RingChi2Mom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getRingChi2(); },
                {binning(250, -2000, 3000), binning(100, 0, 500)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RingNumCalsMom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getRingNumCals(); },
                {binning(250, -2000, 3000), binning(50, 0, 50)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RingRadiusMom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return c->getRingRadius(); },
                {binning(250, -2000, 3000), binning(70, 0, 35)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaMom", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(250, -2000, 3000), binning(100, 0, 10)})));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaMomLowR", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(250, -2000, 3000), binning(100, 0, 10)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() < 13; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaMomMidR", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(250, -2000, 3000), binning(100, 0, 10)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() >= 13 && c->getRingRadius() < 17; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaMomHigR", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncMomCharge,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(250, -2000, 3000), binning(100, 0, 10)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() >= 17; })));

            auto FncRichQaBefore = [](const HParticleCand *, const ParticleEmission em) -> float
                { return em.richQa; };
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaBeforeAfterLowR", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncRichQaBefore,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(100, 0, 10), binning(100, 0, 10)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() < 13; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaBeforeAfterMidR", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncRichQaBefore,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(100, 0, 10), binning(100, 0, 10)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() >= 13 && c->getRingRadius() < 17; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "RichQaBeforeAfterHigR", whatkind, histKindSysFormatter, hist2DSysCreator,
                FncRichQaBefore,
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getRichMatchingQuality(); },
                {binning(100, 0, 10), binning(100, 0, 10)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() >= 17; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "ZBeforeAfterLowR", whatkind, histKindSysFormatter, hist2DSysCreator,
                [](const HParticleCand *, const ParticleEmission em) -> float
                { return em.z; },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getZ(); },
                {binning(400, -200, 100), binning(400, -200, 100)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() < 13; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "ZBeforeAfterMidR", whatkind, histKindSysFormatter, hist2DSysCreator,
                [](const HParticleCand *, const ParticleEmission em) -> float
                { return em.z; },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getZ(); },
                {binning(400, -200, 100), binning(400, -200, 100)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() >= 13 && c->getRingRadius() < 17; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "ZBeforeAfterHigR", whatkind, histKindSysFormatter, hist2DSysCreator,
                [](const HParticleCand *, const ParticleEmission em) -> float
                { return em.z; },
                [](const HParticleCand *c, const ParticleEmission) -> float
                { return const_cast<HParticleCand *>(c)->getZ(); },
                {binning(400, -200, 100), binning(400, -200, 100)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() >= 17; })));
            hists[whatkind].push_back(std::shared_ptr<IHist>(new SingleHist2D<HParticleCand>(
                hM, "ZRHigR", whatkind, histKindSysFormatter, hist2DSysCreator,
                [](const HParticleCand *, const ParticleEmission em) -> float { return em.z; },
                [](const HParticleCand *, const ParticleEmission em) -> float { return em.r; },
                {binning(400, -200, 100), binning(400, 0, 100)},
                [](const HParticleCand *c) -> bool
                { return c->getRingRadius() >= 17; })));
        }
        void fillSingle(HParticleCand *cand, ParticleEmission emission, const char *whatkind)
        {
            for (auto &h : hists[whatkind])
            {
                h->fill(cand, emission, whatkind);
            }
        }
    };

}

#endif // H_DILEPTON_ANALYSIS

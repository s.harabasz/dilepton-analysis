#ifndef SELECT_FUNCTIONS
#define SELECT_FUNCTIONS

//------------------------------------------------------------
//-- NEEDED GLOBAL POINTERS ----------------------------------
//------------------------------------------------------------
static const int NWEIGHTS = 1;
static TH1F *mlplocmin_sys0 = NULL;
static TH1F *mlplocmin_sys1 = NULL;
static TH1F *hcutRichQaSys0 = NULL;
static TH1F *hcutRichQaSys1 = NULL;
static TH1F *hcutMassSys0 = NULL;
static TH1F *hcutMassSys1 = NULL;
static map<Int_t, Float_t> MLPs;
static map<Int_t, Float_t> angleNearestBoth;
static map<Int_t, Float_t> angleNearestFull;
static map<Int_t, Float_t> angleNearestSeg;
static map<Int_t, Int_t>   nearestIndexSeg;

template <typename T>
Int_t findNearestNeighbor(HParticleCand *cand1, T &angleNearestBoth, T &angleNearestSeg, T &angleNearestFull, HCategory *candCat);

void setupMassCuts(TString filename) {
    TFile *fileCutMass = new TFile(filename);
    if (fileCutMass) {
        hcutMassSys0 = (TH1F*)fileCutMass->Get("m2_cut_sys0");
        hcutMassSys1 = (TH1F*)fileCutMass->Get("m2_cut_sys1");
        if (!hcutMassSys0 || !hcutMassSys1) {
            cout << "No histogram with mass cuts found in file. Exitting..." << endl;
            exit(-1);
        }
    }
    else {
        cout << "No file with mass cuts found. Exitting..." << endl;
        exit(-1);
    }
 //   hcutMassSys1->Scale(0.25);
}

void setupRichCuts(TString filename) {
    TFile *fileCutRichQa = new TFile(filename);
    if (fileCutRichQa) {
        hcutRichQaSys0 = (TH1F*)fileCutRichQa->Get("hdeltaRichQaP_mlp_sys0_purity94");
        hcutRichQaSys1 = (TH1F*)fileCutRichQa->Get("hdeltaRichQaP_mlp_sys1_purity94");
        if (!hcutRichQaSys0 || !hcutRichQaSys1) {
            cout << "No histogram with RICH QA cuts found in file. Exitting..." << endl;
            exit(-1);
        }
    }
    else {
        cout << "No file with RICH QA cuts found. Exitting..." << endl;
        exit(-1);
    }
}


void setupMLPCuts(TString filename) {
    TFile *fileCutMLP = new TFile(filename);
    if (fileCutMLP) {
        mlplocmin_sys0 = (TH1F*)fileCutMLP->Get("mlplocmin_sys0");
        mlplocmin_sys1 = (TH1F*)fileCutMLP->Get("mlplocmin_sys1");
//        mlplocmin_sys0 = (TH1F*)fileCutMLP->Get("mlplocmin_MLPMom_cutRich_sys0_w1");
//        mlplocmin_sys1 = (TH1F*)fileCutMLP->Get("mlplocmin_MLPMom_cutRich_sys1_w1");
        if (!mlplocmin_sys0 || !mlplocmin_sys1) {
            cout << "No histogram with MLP cuts found in file. Exitting..." << endl;
            exit(-1);
        }
    }
    else {
        cout << "No file with MLP cuts found. Exitting..." << endl;
        exit(-1);
    }
}

#endif
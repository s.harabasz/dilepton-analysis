#include "hloop.h"
#include "htool.h"

#include "TString.h"
#include "TSystem.h"
#include "TFile.h"
#include "TDatime.h"
#include "TStopwatch.h"
#include "TObjArray.h"

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
using namespace std;

//############# USAGE ##############################
//
// example simple loop:
//
//#include "HLoopPerformance.h"
//
//Int_t loopDST(
//	      TString infileList="/lustre/hebe/hades/dst/mar19/gen2/ag158ag/3200A/081/root/be1908107404*",
//	      TString outfile="test.root",Int_t nEvents=100000000)
//{
//    //  infileList : comma seprated file list "file1.root,file2.root" or "something*.root"
//    //  outfile    : optional (not used here) , used to store hists in root file
//    //  nEvents    : number of events to processed. if  nEvents < entries or < 0 the chain will be processed
//
//    //-------------------------------------------------
//    // create loop obejct and hades
//    HLoop loop(kTRUE);
//    //-------------------------------------------------
//
//
//    //-------------------------------------------------
//    // reading input files and decalring containers
//     Bool_t ret =kFALSE;
//    if(infileList.Contains(",")){
//	ret = loop.addMultFiles(infileList);  // file1,file2,file3
//    } else{
//	ret = loop.addFiles(infileList);      // myroot*.root
//    }
//
//    if(ret == 0) {
//	cout<<"READBACK: ERROR : cannot find inputfiles : "<<infileList.Data()<<endl;
//	return 1;
//    }
//
//    if(!loop.setInput("-*,+HParticleCand,+HParticleEvtInfo")) {  // select some Categories
//	cout<<"READBACK: ERROR : cannot read input !"<<endl;
//	exit(1);
//    } // read all categories
//    loop.printCategories();
//    loop.printChain();
//    //-------------------------------------------------
//
//    HCategory* evtInfoCat = (HCategory*)HCategoryManager::getCategory(catParticleEvtInfo);
//    //-------------------------------------------------
//
//    //-------------------------------------------------
//    // log the performance of analysis
//    HLoopPerformance  perf(outfile);
//    //-------------------------------------------------
//
//    //-------------------------------------------------
//    // event loop starts here
//    Int_t entries = loop.getEntries();
//    if(nEvents < entries && nEvents >= 0 ) entries = nEvents;
//    TString filename;
//    Int_t sectors [6];
//
//    for (Int_t i = 0; i < entries; i++) {
//	Int_t nbytes =  loop.nextEvent(i);             // get next event. categories will be cleared before
//	if(nbytes <= 0) { cout<<nbytes<<endl; break; } // last event reached
//	if(i%1000 == 0) {cout<<"event "<<i<<endl;}
//
//	//-------------------------------------------------
//      // performance data collection and  prints
//      // place execute berfor any skipping of events
//	perf.execute();
//	//-------------------------------------------------
//
//
//	//-------------------------------------------------
//        // summary event info object
//	HParticleEvtInfo* evtInfo=0;
//      evtInfo = HCategoryManager::getObject(evtInfo,evtInfoCat,0 );
//
//	if(evtInfo&&!evtInfo->isGoodEvent(Particle::kGoodTRIGGER|          // standard event selection mar19
//					  Particle::kGoodVertexClust|
//					  Particle::kGoodVertexCand|
//					  Particle::kGoodSTART|
//					  Particle::kNoPileUpSTART|
//					  Particle::kNoVETO|
//					  Particle::kGoodSTARTVETO|
//					  Particle::kGoodSTARTMETA
//					 )) continue;
//
//      // do your analysis here
//
//  } // end eventloop
//
//    //-------------------------------------------------
//    // print performance per file file and write txt file
//    perf.printList();
//    perf.writeFile();
//    //-------------------------------------------------
//
//    delete gHades;
//    return 0;
//}

struct performance {   // per file

    TString file;      // file,ost,server
    TString ost;       // ost
    TString server;    // server
    TString dateSTART; // date of start reading file
    TString dateSTOP;  // date of stop reading file
    UInt_t  start;     // time_t
    UInt_t  stop;      // time_t
    Double_t CPU;      // CPU time [s]
    Double_t REAL;     // REAL time [s]
    Double_t memRes;   //
    Double_t memVirt;  //
    Double_t io;       //
    TString hostname;  // host running analysis
    Int_t pid;         // process id on host
    Int_t nFHandle;    // open files on /lustre for this process
    Int_t uJobs;       // n jobs running by this user
    Int_t allJobs;     // all jobs running
    Long64_t slurmjobid;
    Long64_t slurmarrayjobid ;
    Long64_t slurmarraytaskid;
    Double_t bytesRead; // bytes from file size



    void fill(TString& fname, Double_t real,Double_t cpu,TDatime& startin,TDatime& stopin,Long64_t bytes)
    {   // fill all perfromance data per file

	file = fname;

        file=gSystem->GetFromPipe(Form("./lustre-find-ost-for-file %s",file.Data()));


	TObjArray* a = file.Tokenize(" ");
	if(a && a->GetEntries()==3){
          file   = ((TObjString*)a->At(0))->GetString();
          ost    = ((TObjString*)a->At(1))->GetString();
          server = ((TObjString*)a->At(2))->GetString();


	} else {
          file   = "unknown";
          ost    = "unknown";
          server = "unknown";

	}

	if(a) delete a;

	ProcInfo_t procinfo;
	MemInfo_t meminfo;

	gSystem->GetProcInfo(&procinfo);
	gSystem->GetMemInfo(&meminfo);
        TString tmp;
        tmp = gSystem->Getenv("SLURM_JOBID");
	slurmjobid      = tmp.Atoll();
	tmp = gSystem->Getenv("SLURM_ARRAY_JOB_ID");
	slurmarrayjobid = tmp.Atoll();
	tmp = gSystem->Getenv("SLURM_ARRAY_TASK_ID");
	slurmarraytaskid= tmp.Atoll();;


	pid             = gSystem->GetPid();
	TString user    = gSystem->Getenv("USER");

        tmp=gSystem->GetFromPipe(Form("lsof -p %i | grep /lustre | wc -l",pid));
        nFHandle=tmp.Atoi();
        /*
	tmp=gSystem->GetFromPipe(Form("squeue -u %s -t R |  grep -v PARTITION | wc -l",user.Data()));
        uJobs=tmp.Atoi();
        tmp=gSystem->GetFromPipe("squeue  -t R |  grep -v PARTITION | wc -l");
        allJobs=tmp.Atoi();
	*/
	uJobs = 0;
	allJobs=0;

	hostname        = gSystem->HostName();
	CPU             = cpu;
	REAL            = real;
        memRes          = (procinfo.fMemResident/1e3);
        memVirt         = (procinfo.fMemVirtual/1e3);
	dateSTART       = startin.AsString();
	dateSTOP        = stopin.AsString();
        start           = startin.Convert();
        stop            = stopin.Convert();
	bytesRead       = (bytes/1e6);
        io              = (bytesRead)/REAL;
    }

    void print()
    {
        // print performance data
	cout<<"---------------------------------------------------"<<endl;
	cout<<"#Performance:"<<file<<","<<ost<<","<<server<<endl;
	cout<<"#Performance:"<<" host "<<hostname<<" pid "<<pid <<endl;
	cout<<"#Performance:"<<" MemRes [MB]       "<<memRes<<" MemVirt "<<memVirt <<endl;
	cout<<"#Performance:"<<" File RealTime [s] "<<REAL<<" Cpu time "<<CPU<<endl;
	cout<<"#Performance:"<<" SLURM_JOBID:      "<<slurmjobid<<" SLURM_ARRAY_JOB_ID: "<<slurmarrayjobid<<" SLURM_ARRAY_TASK_ID: "<<slurmarraytaskid <<endl;
	cout<<"#Performance:"<<" Start "<<dateSTART<<" "<<start<<endl;
	cout<<"#Performance:"<<" Stop  "<<dateSTOP<<" "<<stop<<endl;
	cout<<"#Performance:"<<" Mb "<<bytesRead<<" IO SPEED "<<io<<endl;
	cout<<"#Performance:"<<" nFHandle "<<nFHandle<<" uJobs "<<uJobs<<" allJobs "<<allJobs<<endl;
    }

    void printLong()
    {   // print performance data in one line
	cout<<"#Performance:"<<file<<","<<ost<<","<<server<<","
	    <<hostname<<","<<pid <<","
	    <<memRes<<","<<memVirt<<","
	    <<REAL<<","<<CPU<<","
	    <<dateSTART<<","<<dateSTOP<<","
	    <<start<<","<<stop<<","
	    <<bytesRead<<","<<io<<","
	    <<slurmjobid<<","<<slurmarrayjobid<<","<<slurmarraytaskid<<","
	    <<nFHandle<<","<<uJobs<<","<<allJobs<<endl;
    }

    void writeFile(ofstream& out)
    {
        // write perfromance data in one line to file
	out<<file<<","<<ost<<","<<server<<","
	    <<hostname<<","<<pid <<","
	    <<memRes<<","<<memVirt<<","
	    <<REAL<<","<<CPU<<","
	    <<dateSTART<<","<<dateSTOP<<","
	    <<start<<","<<stop<<","
	    <<bytesRead<<","<<io<<","
	    <<slurmjobid<<","<<slurmarrayjobid<<","<<slurmarraytaskid<<","
	    <<nFHandle<<","<<uJobs<<","<<allJobs<<endl;
    }
};

class HLoopPerformance {


    public:



    //--------------------------------------------------------------
    TString  outfile;     // write performance to ascii file
    Long64_t fSize ;
    Long64_t fSizeOld ;   // remember previous file size
    TString  oldFilename; // remember previous filename
    TDatime  dateStart;   // get date stamp
    TDatime  dateStop;    // get date stamp
    TStopwatch timer;
    Long64_t evtCount;
    //--------------------------------------------------------------


    vector<performance> vperformance; // store all performance objects
    map<TString,vector<UInt_t> > mServer;
    map<TString,vector<UInt_t> > mOST;
    map<TString,vector<UInt_t> > mHost;


    TFile* f;                         // pointer to current file




    HLoopPerformance(TString out)
    {
	 fSize = 0;
	 fSizeOld = 0;
	 f=0;
         evtCount = 0;
	 outfile=out;
	 outfile.ReplaceAll(".root","_performance.txt");

         vperformance.reserve(5000);
    }

    ~HLoopPerformance()
    {

    }

    static Bool_t sortIO(performance& a, performance& b){
	return a.io < b.io;
    }

    static Bool_t sortStart(performance& a, performance& b){
	return a.start < b.start;
    }


    void execute()
    {
	evtCount++;
        TString filename;
	f     = gLoop->getChain()->GetFile();
	Int_t entries = gLoop->getEntries();
	fSize = f->GetSize();


	if(gLoop->isNewFile(filename)){ // if the actual filename is needed

	    //--------------------------------------------------------------
	    if(oldFilename!=""){
		timer.Stop();
		dateStop.Set();
		performance perf;

		perf.fill(oldFilename, timer.RealTime(),timer.CpuTime(),dateStart, dateStop, fSizeOld);
		perf.print();

		vperformance.push_back(perf);
	    }

	    timer.Reset();
	    timer.Start();
	    dateStart.Set();

	    oldFilename = filename;
	    fSizeOld    = fSize ;

	    //--------------------------------------------------------------
	}

	if(evtCount == entries){ // last file
	    timer.Stop();
	    dateStop.Set();
	    performance perf;

	    perf.fill(oldFilename, timer.RealTime(),timer.CpuTime(),dateStart, dateStop, fSizeOld);
	    perf.print();

	    vperformance.push_back(perf);
	}


    }

    void printHeader(){
        // print header info for one line version
	cout<<"---------------------------------------------------"<<endl;
	cout<<"#Performance:file ost server host pid MemRes [Mb] MemVirt [Mb] RealTime [s] CPUTime [s] Start Stop Start Stop DataRead [Mb] IOSpeed [Mb/s] SLURM_JOBID SLURM_ARRAY_JOB_ID SLURM_ARRAY_TASK_ID"<<endl;
    }


    void printList()
    {
        // loop over list of runs and print with one line per file
	if(vperformance.size()>0){
	    printHeader();
	    for(UInt_t i=0;i<vperformance.size();i++){
		vperformance[i].printLong();

	    }

	}
    }

    void writeFile()
    {
        // write
	ofstream out;
	out.open(outfile.Data());
	out<<"#file ost server host pid MemRes [Mb] MemVirt [Mb] RealTime [s] CPUTime [s] Start Stop Start Stop DataRead [Mb] IOSpeed [Mb/s] SLURM_JOBID SLURM_ARRAY_JOB_ID SLURM_ARRAY_TASK_ID"<<endl;
	for(UInt_t i=0;i<vperformance.size();i++){
	    vperformance[i].writeFile(out);
	}
        out.close();
    }

    void readFiles(TString expression)
    {
	TObjArray* arr = HTool::glob(expression);
	Int_t n = arr->GetEntries();
	if(n == 0) {
	    Warning("readFiles()","No file matching expression '%s' found !",expression.Data());
	} else {

            ifstream in;
	    Char_t line[2000];
	    TString l;
	    for(Int_t i = 0; i < n; i ++){
		TString name = ((TObjString*)arr->At(i))->GetString();
                cout<<i<<" "<<name<<endl;
                performance perf;
		in.open(name.Data());
		while(!in.eof() && in.good()){
		    in.getline(line,2000);
                    if(!in.good()) break;
		    l = line;
		    if(l.BeginsWith("#")) continue;
		    l.ReplaceAll(" ","#");

                    TObjArray* a = l.Tokenize(",");

                    
		    if(a ){
                        Int_t n = a ->GetEntries();

			if(n==21){
                            //cout<<"reading v4"<<endl;
			    perf.file            = ((TObjString*)a->At(0)) ->GetString();
			    perf.ost             = ((TObjString*)a->At(1)) ->GetString();
			    perf.server          = ((TObjString*)a->At(2)) ->GetString();
			    perf.hostname        = ((TObjString*)a->At(3)) ->GetString();
			    perf.pid             =(((TObjString*)a->At(4)) ->GetString()).Atoi();
			    perf.memRes          =(((TObjString*)a->At(5)) ->GetString()).Atof();
			    perf.memVirt         =(((TObjString*)a->At(6)) ->GetString()).Atof();
			    perf.REAL            =(((TObjString*)a->At(7)) ->GetString()).Atof();
			    perf.CPU             =(((TObjString*)a->At(8)) ->GetString()).Atof();
			    perf.dateSTART       = ((TObjString*)a->At(9)) ->GetString();
			    perf.dateSTOP        = ((TObjString*)a->At(10))->GetString();
			    perf.dateSTART.ReplaceAll("#"," ");
			    perf.dateSTOP.ReplaceAll("#"," ");

			    perf.start           =(((TObjString*)a->At(11))->GetString()).Atoi();
			    perf.stop            =(((TObjString*)a->At(12))->GetString()).Atoi();
			    perf.bytesRead       =(((TObjString*)a->At(13))->GetString()).Atof();
			    perf.io              =(((TObjString*)a->At(14))->GetString()).Atof();
			    perf.slurmjobid      =(((TObjString*)a->At(15))->GetString()).Atoi();
			    perf.slurmarrayjobid =(((TObjString*)a->At(16))->GetString()).Atoi();
			    perf.slurmarraytaskid=(((TObjString*)a->At(17))->GetString()).Atoi();

			    perf.nFHandle        =(((TObjString*)a->At(18))->GetString()).Atoi();
			    perf.uJobs           =(((TObjString*)a->At(19))->GetString()).Atoi();
			    perf.allJobs         =(((TObjString*)a->At(20))->GetString()).Atoi();

			} else if(n==18){
			    //cout<<"reading v3"<<endl;
			    perf.file            = ((TObjString*)a->At(0)) ->GetString();
			    perf.ost             = ((TObjString*)a->At(1)) ->GetString();
			    perf.server          = ((TObjString*)a->At(2)) ->GetString();
			    perf.hostname        = ((TObjString*)a->At(3)) ->GetString();
			    perf.pid             =(((TObjString*)a->At(4)) ->GetString()).Atoi();
			    perf.memRes          =(((TObjString*)a->At(5)) ->GetString()).Atof();
			    perf.memVirt         =(((TObjString*)a->At(6)) ->GetString()).Atof();
			    perf.REAL            =(((TObjString*)a->At(7)) ->GetString()).Atof();
			    perf.CPU             =(((TObjString*)a->At(8)) ->GetString()).Atof();
			    perf.dateSTART       = ((TObjString*)a->At(9)) ->GetString();
			    perf.dateSTOP        = ((TObjString*)a->At(10))->GetString();
			    perf.dateSTART.ReplaceAll("#"," ");
			    perf.dateSTOP.ReplaceAll("#"," ");

			    perf.start           =(((TObjString*)a->At(11))->GetString()).Atoi();
			    perf.stop            =(((TObjString*)a->At(12))->GetString()).Atoi();
			    perf.bytesRead       =(((TObjString*)a->At(13))->GetString()).Atof();
			    perf.io              =(((TObjString*)a->At(14))->GetString()).Atof();
			    perf.slurmjobid      =(((TObjString*)a->At(15))->GetString()).Atoi();
			    perf.slurmarrayjobid =(((TObjString*)a->At(16))->GetString()).Atoi();
			    perf.slurmarraytaskid=(((TObjString*)a->At(17))->GetString()).Atoi();

			} else if (n==16){
                            //cout<<"reading v2"<<endl;
			    // old format
			    perf.file            = ((TObjString*)a->At(0)) ->GetString();
			    perf.ost             = ((TObjString*)a->At(1)) ->GetString();
			    perf.server          = ((TObjString*)a->At(2)) ->GetString();
			    perf.hostname        = ((TObjString*)a->At(3)) ->GetString();
			    perf.pid             =(((TObjString*)a->At(4)) ->GetString()).Atoi();
			    perf.memRes          =(((TObjString*)a->At(5)) ->GetString()).Atof();
			    perf.memVirt         =(((TObjString*)a->At(6)) ->GetString()).Atof();
			    perf.REAL            =(((TObjString*)a->At(7)) ->GetString()).Atof();
			    perf.CPU             =(((TObjString*)a->At(8)) ->GetString()).Atof();
			    perf.dateSTART       = ((TObjString*)a->At(9)) ->GetString();
			    perf.dateSTOP        = ((TObjString*)a->At(10))->GetString();
			    perf.dateSTART.ReplaceAll("#"," ");
			    perf.dateSTOP.ReplaceAll("#"," ");
			    perf.bytesRead       =(((TObjString*)a->At(11))->GetString()).Atof();
			    perf.io              =(((TObjString*)a->At(12))->GetString()).Atof();
			    perf.slurmjobid      =(((TObjString*)a->At(13))->GetString()).Atoi();
			    perf.slurmarrayjobid =(((TObjString*)a->At(14))->GetString()).Atoi();
			    perf.slurmarraytaskid=(((TObjString*)a->At(15))->GetString()).Atoi();
			}

			//perf.printLong();
                        vperformance.push_back(perf);
			delete a;
		    }
		}
		in.close();

                

	    }

	    cout<<"#Performance:readFiles(): read "<<vperformance.size()<<" file infos!"<<endl;
	    sort(vperformance.begin(),vperformance.end(),sortIO);
	    cout<<"#Performance: finished sorting!"<<endl;

	    map<TString,vector<UInt_t> >::iterator it;
	    for(UInt_t i=0;i<vperformance.size();i++){
		performance& perf = vperformance[i];


                //-----------------------------------
		// server
		it = mServer.find(perf.server);
		if(it == mServer.end()) { // new
		    vector<UInt_t> v;
		    v.push_back(i);
                    mServer[perf.server] = v;
		} else { // add to list
		    vector<UInt_t>& v = it->second;
                    v.push_back(i);
		}
                //-----------------------------------

		//-----------------------------------
		// ost
		it = mOST.find(perf.ost);
		if(it == mOST.end()) { // new
		    vector<UInt_t> v;
		    v.push_back(i);
                    mOST[perf.ost] = v;
		} else { // add to list
		    vector<UInt_t>& v = it->second;
                    v.push_back(i);
		}
                //-----------------------------------

		//-----------------------------------
		// host
		it = mHost.find(perf.hostname);
		if(it == mHost.end()) { // new
		    vector<UInt_t> v;
		    v.push_back(i);
                    mHost[perf.hostname] = v;
		} else { // add to list
		    vector<UInt_t>& v = it->second;
                    v.push_back(i);
		}
                //-----------------------------------


	    }




	    cout<<"#Performance: N server = "<<mServer.size()<<" n OST = "<<mOST.size()<<" n Host "<<mHost.size()<<endl;
            //printList();


	}
	if(arr) delete arr;


    }


};


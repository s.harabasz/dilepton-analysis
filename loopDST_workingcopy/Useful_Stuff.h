#include "hrichhit.h"
#include "hrichcal.h"

// -------------------------------------------------------------------
// -------------------------------------------------------------------
// This file contains all used functions for analysis macro
// - Lepton selection Cut functions
// - efficiency correction functions
// -------------------------------------------------------------------
// For systematic studies use (one of) the following variations:
// - Change of the Comversion supression Cut from 25 Cals to 35
// - Change of RICH matching cut to momentum independent at 2 degree
// - Change of M2 cut to momentum independent at 10k MeV2
// -------------------------------------------------------------------
// -------------------------------------------------------------------

Bool_t isInnerPMTPlane(HRichHit* ring)
{
    Double_t Posx = ring->getRich700CircleCenterX();
    Double_t Posy = ring->getRich700CircleCenterY();

    if (Posx < 350 && Posx > -350 && Posy < 350 && Posy > -350) return true;
    else return false;
}


Bool_t isConversionCutDiff(HParticleCand* cand, HCategory* RingCat, HCategory* CalCat)
{
    // Function to remove candidates matched with ring
    // with many other Cals around
    // a) Double Ring - Conversion
    // b) Blob - unphysical
    // Cut is based on difference between Ring_Cals and Cals in 3Rad area around Ring center

    Int_t richIndex = cand->getRichInd();
    HRichHit* ring = static_cast<HRichHit*>(RingCat->getObject(richIndex));
    Double_t Posx = ring->getRich700CircleCenterX();
    Double_t Posy = ring->getRich700CircleCenterY();
    Double_t Radius = ring->getRich700CircleRadius();

    Int_t N_ofCals = ring->getRich700NofRichCals();
    Int_t Cal_Counter = 0;
    Int_t size = CalCat->getEntries();
    HRichCal* Cal =0;
    for (Int_t i=0; i < size; i++) {
	Cal = HCategoryManager::getObject(Cal,CalCat,i);
        Double_t x_coord = (Cal->getCol()-96)*6.625;       // translate from col/row in mm
	Double_t y_coord = (Cal->getRow()-96)*6.625;

        Double_t dist_center = sqrt((Posx-x_coord)*(Posx-x_coord)+(Posy-y_coord)*(Posy-y_coord));
        if (dist_center<(2*Radius)) Cal_Counter++;
    }
    //cout << Radius << "  " << Cal_Counter << "    " << N_ofCals << endl;
    if ((Cal_Counter-N_ofCals)<15) return false;
    return true;
}


Bool_t isConversion(HParticleCand* cand, HCategory* RingCat, HCategory* CalCat, Int_t &cal_counter)
{
    // Function to remove candidates matched with ring
    // with many other Cals around
    // a) Double Ring - Conversion
    // b) Blob - unphysical
    // Cut is only based on Cals in 3Rad area around ring center - double peak structure in spectrum for tweo rings

    Int_t richIndex = cand->getRichInd();
    HRichHit* ring = static_cast<HRichHit*>(RingCat->getObject(richIndex));
    Double_t Posx = ring->getRich700CircleCenterX();
    Double_t Posy = ring->getRich700CircleCenterY();
    Double_t Radius = ring->getRich700CircleRadius();

    Int_t Cal_Counter = 0;
    Int_t size = CalCat->getEntries();
    for (Int_t i=0; i < size; i++) {
	//Cal = HCategoryManager::getObject(Cal,CalCat,i);
        HRichCal* Cal = static_cast<HRichCal*>(CalCat->getObject(i));
        Double_t x_coord = (Cal->getCol()-96)*6.625;       // translate from col/row in mm
	Double_t y_coord = (Cal->getRow()-96)*6.625;

        Double_t dist_center = sqrt((Posx-x_coord)*(Posx-x_coord)+(Posy-y_coord)*(Posy-y_coord));
        if (dist_center<(2*Radius)) Cal_Counter++;
    }
    cal_counter = Cal_Counter;
    if (Cal_Counter<25) return false;    // 25 original, 30 for consisteny check
    return true;
}


Bool_t isM2inRange(HParticleCand* cand)
{
    //Parameter extracted from EstimateMassCut.C
    Double_t EqPos, EqEl;

 //   EqPos = 12709.2 - 7.43282*Mom;
 //   EqEl = -12738 + 10.1206*Mom;

    // systematic study: constant cut value
    EqPos = 10000;
    EqEl  = -10000;

    if (cand-> getCharge()>0) {
	if (cand ->getMass2() > EqPos) return false;
    }
    if (cand-> getCharge()<0) {
	if (cand ->getMass2() * cand ->getCharge() < EqEl) return false;
    }

    return true;
}


Bool_t isRichMatchOK(HParticleCand* cand, Double_t correction)
{
    //Parameter extracted from EstimateRICHMomCut.C
    Double_t Rich_Matching = TMath::Sqrt((cand->getDeltaTheta()-correction)*(cand->getDeltaTheta()-correction) + cand->getDeltaPhi()*cand->getDeltaPhi());
    if (Rich_Matching > 1.2) return false;        // 2.0 as upper limit for low momenta  , Cut at 1.2 as consistency check
    return true;
}




Bool_t isVertexOK(HParticleCand* cand, Int_t EventVertex)
{
    //Parameter extracted from EstimateVertexCut.C
    Double_t Lower_Corner = -40;             // 40 Gen3, 50 Gen4
    Double_t Upper_Corner = 40;
    if ((cand->getZ()-EventVertex) > Upper_Corner || (cand->getZ()-EventVertex) < Lower_Corner) return false;
    return true;
}


Float_t getdThetaCorrection(HParticleCand* cand, TH2F *ThetaCorrection, HCategory* RingCat)
{
    Int_t richIndex = cand->getRichInd();
    HRichHit* ring = static_cast<HRichHit*>(RingCat->getObject(richIndex));
    //Calculate ThetaCorrection from Histogram and Ring matched to Candidate
    Double_t Posx = ring->getRich700CircleCenterX();
    Double_t Posy = ring->getRich700CircleCenterY();

    Double_t Binning = ThetaCorrection->GetNbinsX();
    Double_t startBin=-600, endBin=600;
    Double_t binRange = (endBin-startBin)/Binning;

    Int_t Binx = (Int_t)(Posx-startBin)/binRange;
    Int_t Biny = (Int_t)(Posy-startBin)/binRange;

    //cout << Binx << "  " << Biny << endl;

    return (ThetaCorrection->GetBinContent(Binx,Biny));
}





Bool_t isConvOA(HParticleCand* cand, HCategory* candCat, Double_t EventVtx, TH2F* ThetaCorr, HCategory* RingCat){
    Bool_t pairfound = false;
    Int_t size = candCat->getEntries();
    for(Int_t j = 0; j < size && pairfound == false; j++)
    {
	//candX = HCategoryManager::getObject(candX,candCat,j);
        HParticleCand* candX = static_cast<HParticleCand*>(candCat->getObject(j));
	if(candX) {
            if (cand->getCharge() == candX->getCharge()) continue;
	   // if(!candX->isFlagBit(kIsUsed)) { continue; }           // skip particles rejected by HParticleTrackSorter
	    if(!candX->isFlagBit(kIsLepton)) { continue; }
            candX->calc4vectorProperties(HPhysicsConstants::mass(3));
	    if(!isVertexOK(candX, EventVtx)) continue;
          //  if (!isM2inRange(candX)) continue;
          //  if (!isRichMatchOK(candX, getdThetaCorrection(candX, ThetaCorr,RingCat))) continue;
	  //  if (candX->getRingRadius()<18.7473) continue;

	    Float_t opening_angle = HParticleTool::getOpeningAngle(candX, cand);
	    //cout << "oa:  " <<  opening_angle << endl;
	    if (opening_angle > 9) continue;
            else pairfound = true;
	}
    }
   // cout << "found:  "  << pairfound << endl;
    return pairfound;
}


Bool_t isOARing(HParticleCand* cand, HCategory* RingCat){
    Bool_t pairfound = false;
    Double_t Radius_Cut = 18.7473;
    Int_t richIndex = cand -> getRichInd();
    HRichHit* ring = static_cast<HRichHit*>(RingCat->getObject(richIndex));

    Int_t size = RingCat->getEntries();
    for(Int_t j = 0; j < size && pairfound == false; j++)
    {
	if (j == richIndex) continue;
	HRichHit* ringref = static_cast<HRichHit*>(RingCat->getObject(j));
        if (ringref->getRich700CircleRadius()<Radius_Cut) continue;
	Double_t Theta1 = (TMath::Pi() / 180.0) * ring->getTheta();
	Double_t Theta2 = (TMath::Pi() / 180.0) * ringref->getTheta();
        Double_t Phi1 = (TMath::Pi() / 180.0) * ring->getPhi();
        Double_t Phi2 = (TMath::Pi() / 180.0) * ringref->getPhi();


	Double_t opening_angle = (180.0 / TMath::Pi()) * TMath::ACos(TMath::Sin(Theta1)*TMath::Sin(Theta2)*TMath::Cos(Phi1 - Phi2)+TMath::Cos(Theta1)*TMath::Cos(Theta2));

        //cout << "oa:  " <<  opening_angle << endl;
	if (opening_angle > 9) continue;
	else pairfound = true;
    }
    //cout << pairfound << endl;
    return pairfound;
}




Int_t getClosestLayer(Double_t VertexEvt)
{
    //Calculate Target Layer for ThetaCorrection from Vertex Position
    //needed for dTheta correction
    Double_t TargetLayer;
    if(VertexEvt>-10.00 || VertexEvt<-64.50) TargetLayer = -1;
    if(VertexEvt>-64.50) TargetLayer = 1;
    if(VertexEvt>-60.23) TargetLayer = 2;
    if(VertexEvt>-56.52) TargetLayer = 3;
    if(VertexEvt>-53.00) TargetLayer = 4;
    if(VertexEvt>-49.38) TargetLayer = 5;
    if(VertexEvt>-45.73) TargetLayer = 6;
    if(VertexEvt>-42.32) TargetLayer = 7;
    if(VertexEvt>-38.82) TargetLayer = 8;
    if(VertexEvt>-35.36) TargetLayer = 9;
    if(VertexEvt>-31.58) TargetLayer = 10;
    if(VertexEvt>-27.99) TargetLayer = 11;
    if(VertexEvt>-24.25) TargetLayer = 12;
    if(VertexEvt>-21.13) TargetLayer = 13;
    if(VertexEvt>-17.07) TargetLayer = 14;
    if(VertexEvt>-13.58) TargetLayer = 15;
    return TargetLayer;
}



Float_t getEfficiencyCorrection(HParticleCand* cand, TH3F *EfficiencyCorrectionep, TH3F *EfficiencyCorrectionem)           // for mom, theta, phi correction
{
    Float_t correction;
    Double_t mom = cand->getMomentum()/40;
    Double_t theta = cand->getTheta()/2;
    Double_t phi = cand->getPhi()/4;

    Int_t Binx = (Int_t) mom;         // 50 Bins from 0 to 2000 in mom
    Int_t Biny = (Int_t) theta;         // 45 bins from 0 to 90 in theta
    Int_t Binz = (Int_t) phi;         // 90 bins from 0 to 360 in Phi

    if(cand->getCharge()>0) {
        correction = EfficiencyCorrectionep ->GetBinContent(Binx,Biny,Binz);
    }
    else {
	correction = EfficiencyCorrectionem ->GetBinContent(Binx,Biny,Binz);
    }
    return (correction);
}


Float_t getEfficiencyCorrectionVC(HVirtualCand* cand, TH3F *EfficiencyCorrectionep, TH3F *EfficiencyCorrectionem)           // for mom, theta, phi correction
{
    Float_t correction;
    Double_t mom = cand->getMomentum()/40;
    Double_t theta = cand->getTheta()/2;
    Double_t phi = cand->getPhi()/4;

    Int_t Binx = (Int_t) mom;         // 50 Bins from 0 to 2000 in mom
    Int_t Biny = (Int_t) theta;         // 45 bins from 0 to 90 in theta
    Int_t Binz = (Int_t) phi;         // 90 bins from 0 to 360 in Phi

    if(cand->getCharge()>0) {
        correction = EfficiencyCorrectionep ->GetBinContent(Binx,Biny,Binz);
    }
    else {
	correction = EfficiencyCorrectionem ->GetBinContent(Binx,Biny,Binz);
    }
    return (correction);
}

Bool_t isStatsok(HVirtualCand* cand, TH3F *Recoep, TH3F *Recoem)           // for mom, theta, phi correction
{
    Double_t mom = cand->getMomentum()/40;
    Double_t theta = cand->getTheta()/2;
    Double_t phi = cand->getPhi()/4;

    Int_t Binx = (Int_t) mom;         // 50 Bins from 0 to 2000 in mom
    Int_t Biny = (Int_t) theta;         // 45 bins from 0 to 90 in theta
    Int_t Binz = (Int_t) phi;         // 90 bins from 0 to 360 in Phi

    if (cand->getCharge()>0 && Recoep ->GetBinContent(Binx,Biny,Binz) <8) return false;
    if (cand->getCharge()<0 && Recoem ->GetBinContent(Binx,Biny,Binz) <8) return false;
    return true;
}

Bool_t isStatsok(HParticleCand* cand, TH3F *Recoep, TH3F *Recoem)           // for mom, theta, phi correction
{
    Double_t mom = cand->getMomentum()/40;
    Double_t theta = cand->getTheta()/2;
    Double_t phi = cand->getPhi()/4;

    Int_t Binx = (Int_t) mom;         // 50 Bins from 0 to 2000 in mom
    Int_t Biny = (Int_t) theta;         // 45 bins from 0 to 90 in theta
    Int_t Binz = (Int_t) phi;         // 90 bins from 0 to 360 in Phi

    if (cand->getCharge()>0 && Recoep ->GetBinContent(Binx,Biny,Binz) <8) return false;
    if (cand->getCharge()<0 && Recoem ->GetBinContent(Binx,Biny,Binz) <8) return false;
    return true;
}


Float_t getEfficiencyCorrectiony(HParticleCand* cand, TH2F *EfficiencyCorrectionep, TH2F *EfficiencyCorrectionem)           // for pt_y correction
{
    Double_t correction;
    Double_t rapidity = (cand->Rapidity()+1)*100;
    Double_t transmom = cand->Pt();

    Int_t Binx = (Int_t) rapidity;         // 400 Bins from -1 to 3 in mom
    Int_t Biny = (Int_t) transmom;         // 1000 bins from 0 to 1000 in theta

    if(cand->getCharge()>0) {
        correction = EfficiencyCorrectionep ->GetBinContent(Binx,Biny);
    }
    else {
	correction = EfficiencyCorrectionem ->GetBinContent(Binx,Biny);
    }
    //cout << cand->getCharge()  << "  correction " << correction << endl;
    return (correction);
}


Float_t getEfficiencyCorrectionVCy(HVirtualCand* cand, TH2F *EfficiencyCorrectionep, TH2F *EfficiencyCorrectionem)    // for pt_y correction      
{
    Double_t correction;
    Double_t rapidity = (cand->Rapidity()+1)*100;
    Double_t transmom = cand->Pt();

    Int_t Binx = (Int_t) rapidity;         // 400 Bins from -1 to 3 in mom
    Int_t Biny = (Int_t) transmom;         // 1000 bins from 0 to 1000 in theta

    if(cand->getCharge()>0) {
        correction = EfficiencyCorrectionep ->GetBinContent(Binx,Biny);
    }
    else {
	correction = EfficiencyCorrectionem ->GetBinContent(Binx,Biny);
    }
    return (correction);
}

Bool_t isStatsoky(HVirtualCand* cand, TH2F *Recoep, TH2F *Recoem)          // for pt_y correction 
{
    Double_t rapidity = (cand->Rapidity()+1)*100;
    Double_t transmom = cand->Pt();

    //cout << rapidity << "   " << transmom << endl;

    Int_t Binx = (Int_t) rapidity;         // 400 Bins from -1 to 3 in mom
    Int_t Biny = (Int_t) transmom;         // 1000 bins from 0 to 1000 in theta

    //cout << Binx << "    " << Biny << endl;

    if (cand->getCharge()>0 && Recoep ->GetBinContent(Binx,Biny) <8) return false;
    if (cand->getCharge()<0 && Recoem ->GetBinContent(Binx,Biny) <8) return false;
    return true;
}

Bool_t isStatsoky(HParticleCand* cand, TH2F *Recoep, TH2F *Recoem)          // for pt_y correction 
{
    Double_t rapidity = (cand->Rapidity()+1)*100;
    Double_t transmom = cand->Pt();

    Int_t Binx = (Int_t) rapidity;         // 400 Bins from -1 to 3 in mom
    Int_t Biny = (Int_t) transmom;         // 1000 bins from 0 to 1000 in theta

    if (cand->getCharge()>0 && Recoep ->GetBinContent(Binx,Biny) <8) return false;
    if (cand->getCharge()<0 && Recoem ->GetBinContent(Binx,Biny) <8) return false;
    return true;
}



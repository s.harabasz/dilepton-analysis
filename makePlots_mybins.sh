startDay=$((10#${1}))   #this will make a decimal number from the input (in case user put there 034 -> 34)
startDay=$(printf "%03d" $startDay)  #this will make from decimal number the three digit format in case it is needed 34 -> 034 but 134 -> 134
endDay=$((10#${2}))
endDay=$(printf "%03d" $endDay)
user=$(whoami)
plotDir="plots/online_oaRingNotConversion"
mkdir -p ${plotDir}
inputDir="/lustre/hades/user/${user}/feb22/analysis_exp/online/dilepton_mix"
pairCorr=""
backgroundType="GeometricMean" # GeometricMean or ArithmeticMean
root -l -b -q 'makePlots_mybins.C+g(None,"'"${plotDir}"'", "'"${startDay}"'","'"${endDay}"'","hmass","'"${inputDir}"'","'"${pairCorr}"'",0,"'"${backgroundType}"'")'

# Dilepton analysis

Programs and macros for inclusive dilepton analysis in HADES

### Steps and instructions

#### Get the software

1. Clone the repository, note where the directory `dilepton-analysis` is created:

        git clone https://git.gsi.de/s.harabasz/dilepton-analysis.git

2. Login to virgo, go to the directory `dilepton-analysis`:

        ssh virgo-debian10.hpc.gsi.de
        cd dilepton-analysis

3. Load Hydra:

        . /cvmfs/hadessoft.gsi.de/install/debian10/6.24.02/hydra2-6.3/defall.sh

4. Go to the directory with the source code, and recompile the program (just to make sure):

        cd loopDST_workingcopy
        make
        cd ..

#### Run batchfarm jobs

6. Make sure that you are still on virgo. You can tell this by checking the host name of the machine to which you are logged in. Virgo machines have host names in the format `lxbk` + four digits. Typically you can see the hostname in the command prompt in your command line. It often has the format `username@hostname:/current/directory>`. If you have different configuration, you can also check the hostname by running the command: 

        hostname

Copy up-to-date versions of the official file lists:

        cd loopDST_install
        cp /lustre/hades/dst/feb22/gen2/filelists/day_0??.list lists_feb22

7. Send data analysis jobs to the batchfarm, edit the file `sendScript_SL.sh` and adjust all the file paths in it, then

        . sendScript_SL.sh 033

8. Monitor how the jobs are running, from time to time type:

        squeue -u $(whoami)

To get the number of running jobs:

        squeue -u $(whoami) | grep $(whoami) | grep R | wc -l

To see the list of pending jobs:

        squeue -u $(whoami) | grep PD

9. If after some time the output from this command is empty, then the jobs are done. So start the container again, load Hydra again, go to the directory, where the output files are stored, and merge output files from all the jobs into a single file:

        cd dilepton-analysis
        . /cvmfs/hadessoft.gsi.de/install/debian10/6.24.02/hydra2-6.3/defall.sh
        cd /lustre/hades/user/$(whoami)/feb22/analysis_exp/gen2test2/dilepton/

9 1/2. Merge several output files from single batch farm into one. If there are few files to merge, use:

        hadd 033.root 033/*.root

If there are many, use:

        hadd.pl 033.root 033/*.root

10. If in step 12 you will want to make plots combining data from many days, then now you need to create a combined file with histograms. For example:

        hadd 035_038.root 035.root 036.root 037.root 038.root

#### Produce plots

11. Still being on virgo, go to the `dilepton-analysis` directory

12. Edit the script `makePlots_mybins.sh`, especially write there from which days you want to use the data

13. Run the script, specifying the start and the end day for which you want to make plots, for example:

        . makePlots_mybins.sh 035 038

If you want to make plots for one day, make both number the same, for example:

        . makePlots_mybins.sh 036 036

13. New plots should appear in the subdirectory `plots/gen2test2`. To watch them, you need to be outside of the container and of virgo, because virgo doesn't have capabilities to display graphics. You can use a separate tab of your terminal window for that.

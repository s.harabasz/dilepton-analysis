#include "TString.h"

TString strCent(int mult) {
    TString strCent;
    if (mult == 0) return "All triggers";
    if (mult == 1) return "Multipicity trigger";
    if (mult == 2) return "Minimum bias trigger";
    return "";
}
